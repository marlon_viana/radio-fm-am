package com.radiofmam.interfaces;

import com.radiofmam.item.ItemRadio;

import java.util.ArrayList;

public interface RadioListListener {
    void onStart();
    void onEnd(String success, ArrayList<ItemRadio> arrayList);
}