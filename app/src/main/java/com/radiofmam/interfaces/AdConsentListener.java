package com.radiofmam.interfaces;

public interface AdConsentListener {
    void onConsentUpdate();
}
