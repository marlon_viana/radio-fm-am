package com.radiofmam.interfaces;

import com.radiofmam.item.ItemOnDemandCat;
import com.radiofmam.item.ItemRadio;

import java.util.ArrayList;

public interface HomeListener {
    void onStart();
    void onEnd(String success, ArrayList<ItemRadio> arrayList_featured, ArrayList<ItemRadio> arrayList_mostviewed, ArrayList<ItemOnDemandCat> arrayListOnDemandCat);
}