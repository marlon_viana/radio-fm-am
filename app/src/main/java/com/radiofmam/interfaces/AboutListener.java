package com.radiofmam.interfaces;

public interface AboutListener {
    void onStart();

    void onEnd(String success);
}