package com.radiofmam.interfaces;

import com.radiofmam.item.ItemRadio;

import java.util.ArrayList;

public interface RadioViewListener {
    void onEnd(String success);
}