package com.radiofmam.freebroadcasters;

import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.radiofmam.adapter.AdapterOnDemandCat;
import com.radiofmam.interfaces.InterAdListener;
import com.radiofmam.item.ItemOnDemandCat;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.DBHelper;
import com.radiofmam.utils.JsonUtils;
import com.radiofmam.utils.RecyclerItemClickListener;
import com.radiofmam.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

public class FragmentOnDemandCat extends Fragment {

    DBHelper dbHelper;
    RecyclerView recyclerView;
    AdapterOnDemandCat adapter;
    GridLayoutManager lLayout;
    SearchView searchView;
    ArrayList<ItemOnDemandCat> arraylist;
    CircularProgressBar progressBar;
    TextView textView_empty;
    public static AppCompatButton button_try;
    LinearLayout ll_empty;
    String errr_msg;
    SharedPref sharedPref;
    JsonUtils jsonUtils;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_ondemand_cat, container, false);

        dbHelper = new DBHelper(getActivity());
        jsonUtils = new JsonUtils(getActivity(), interAdListener);
        sharedPref = new SharedPref(getActivity());

        arraylist = new ArrayList<>();
        progressBar = rootView.findViewById(R.id.progressBar_on);

        ll_empty = rootView.findViewById(R.id.ll_empty);
        textView_empty = rootView.findViewById(R.id.textView_empty_msg);
        button_try = rootView.findViewById(R.id.button_empty_try);
        ViewCompat.setBackgroundTintList(button_try, ColorStateList.valueOf(sharedPref.getFirstColor()));

        lLayout = new GridLayoutManager(getActivity(), 2);

        recyclerView = rootView.findViewById(R.id.recyclerView_on);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lLayout);

        loadCity();

        button_try.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadCity();
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                jsonUtils.showInterAd(position, "",0);
            }
        }));

        setHasOptionsMenu(true);
        return rootView;
    }

    private void loadCity() {
        if (JsonUtils.isNetworkAvailable(getActivity())) {
            new MyTask().execute(Constant.URL_ON_DEMAND_CAT);
        } else {
            errr_msg = getString(R.string.internet_not_connected);
            setEmpty();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.search);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);

        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(queryTextListener);
        super.onCreateOptionsMenu(menu, inflater);
    }

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            if (arraylist.size() > 0) {
                if (searchView.isIconified()) {
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    adapter.getFilter().filter(s);
                    adapter.notifyDataSetChanged();
                }
            }
            return true;
        }
    };

    private class MyTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            arraylist.clear();
            ll_empty.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            return JsonUtils.getJSONString(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            if(getActivity()!= null) {
                super.onPostExecute(result);
                if (null == result || result.length() == 0) {
                    jsonUtils.showToast(getString(R.string.items_not_found));
                } else {
                    try {
                        JSONObject mainJson = new JSONObject(result);
                        JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);
                        JSONObject objJson;

                        for (int i = 0; i < jsonArray.length(); i++) {
                            objJson = jsonArray.getJSONObject(i);

                            String id = objJson.getString(Constant.CAT_ID);
                            String name = objJson.getString(Constant.CAT_NAME);
                            String image = objJson.getString(Constant.CAT_IMAGE);
                            String thumb = objJson.getString(Constant.CAT_THUMB);
                            String total_items = objJson.getString(Constant.TOTAL_ITEMS);
                            ItemOnDemandCat objItem = new ItemOnDemandCat(id, name, image, thumb, total_items);
                            arraylist.add(objItem);
                        }
                        errr_msg = getString(R.string.items_not_found);
                    } catch (Exception e) {
                        errr_msg = getString(R.string.server_error);
                        e.printStackTrace();
                    }
                    setAdapterToListview();
                }
            }
        }
    }

    public void setAdapterToListview() {
        adapter = new AdapterOnDemandCat(getActivity(), arraylist);
        recyclerView.setAdapter(adapter);
        setEmpty();
    }

    public void setEmpty() {
        progressBar.setVisibility(View.GONE);
        if (arraylist.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ll_empty.setVisibility(View.GONE);
        } else {
            textView_empty.setText(errr_msg);
            recyclerView.setVisibility(View.GONE);
            ll_empty.setVisibility(View.VISIBLE);
        }
    }

    private InterAdListener interAdListener = new InterAdListener() {
        @Override
        public void onClick(int position, String type) {
           /* int pos = getPosition(adapter.getID(position));
            FragmentManager fm = getFragmentManager();
            FragmentOnDemandDetails f1 = new FragmentOnDemandDetails();
            FragmentTransaction ft = fm.beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putSerializable("item", arraylist.get(pos));
            f1.setArguments(bundle);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.hide(fm.findFragmentByTag(getString(R.string.on_demand)));
            ft.add(R.id.content_frame_activity, f1, arraylist.get(pos).getName());
            ft.addToBackStack(arraylist.get(pos).getName());
            ft.commit();
            ((BaseActivity)getActivity()).getSupportActionBar().setTitle(arraylist.get(pos).getName());*/
        }
    };

    private int getPosition(String id) {
        int count = 0;
        for (int i = 0; i < arraylist.size(); i++) {
            if (id.equals(arraylist.get(i).getId())) {
                count = i;
                break;
            }
        }
        return count;
    }
}