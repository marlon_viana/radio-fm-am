package com.radiofmam.freebroadcasters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.radiofmam.utils.JsonUtils;
import com.radiofmam.utils.SharedPref;

public class FragmentMain extends Fragment {

    SharedPref sharedPref;
    public static AppBarLayout appBarLayout;
    SearchView searchView;
    JsonUtils jsonUtils;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_main, container, false);

        jsonUtils = new JsonUtils(getActivity());
        appBarLayout = rootView.findViewById(R.id.appBar);
        appBarLayout.setBackground(jsonUtils.getGradientDrawableToolbar());

        sharedPref = new SharedPref(getActivity());
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        ViewPager mViewPager = rootView.findViewById(R.id.viewPager_main);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(4);

        TabLayout tabLayout = rootView.findViewById(R.id.tabs_home);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        TabLayout.Tab tab = tabLayout.getTabAt(1);
        tab.select();

        return rootView;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new FragmentCity();
                case 1:
                    return new FragmentRadio();
                case 2:
                    return new FragmentFavorite();
                default:
                    return new FragmentCity();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}