package com.radiofmam.freebroadcasters;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.radiofmam.AsyncTasks.LoadRadioList;
import com.radiofmam.adapter.AdapterCityDetails;
import com.radiofmam.interfaces.RadioListListener;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.JsonUtils;
import com.radiofmam.utils.SharedPref;

import java.util.ArrayList;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;


public class FragmentCityDetails extends Fragment {

    RecyclerView recyclerView;
    ArrayList<ItemRadio> arrayList;
    AdapterCityDetails adapterCityDetails;
    CircularProgressBar progressBar;
    SearchView searchView;
    JsonUtils jsonUtils;
    LoadRadioList loadRadioList;
    TextView textView_empty;
    AppCompatButton button_try;
    LinearLayout ll_empty;
    String errr_msg;
    SharedPref sharedPref;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_city_detail, container, false);

        sharedPref = new SharedPref(getActivity());
        ll_empty = rootView.findViewById(R.id.ll_empty);
        textView_empty = rootView.findViewById(R.id.textView_empty_msg);
        button_try = rootView.findViewById(R.id.button_empty_try);
        ViewCompat.setBackgroundTintList(button_try, ColorStateList.valueOf(sharedPref.getFirstColor()));

        jsonUtils = new JsonUtils(getActivity());

        progressBar = rootView.findViewById(R.id.progressBar_city_details);

        arrayList = new ArrayList<>();

        recyclerView =rootView.findViewById(R.id.recyclerView_city_detail);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        loadRadioByCat();

        button_try.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadRadioByCat();
            }
        });

        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.search);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);

        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(queryTextListener);
        super.onCreateOptionsMenu(menu, inflater);
    }

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {

            if (searchView.isIconified()) {
                recyclerView.setAdapter(adapterCityDetails);
                adapterCityDetails.notifyDataSetChanged();
            } else {
                adapterCityDetails.getFilter().filter(s);
                adapterCityDetails.notifyDataSetChanged();
            }
            return true;
        }
    };

    private void loadRadioByCat() {
        if (JsonUtils.isNetworkAvailable(getActivity())) {
            loadRadioList = new LoadRadioList(new RadioListListener() {
                @Override
                public void onStart() {
                    arrayList.clear();
                    ll_empty.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                public void onEnd(String success, ArrayList<ItemRadio> arrayListFeatured) {
                    if(getActivity() != null) {
                        if (success.equals("1")) {
                            errr_msg = getString(R.string.items_not_found);
                            arrayList.addAll(arrayListFeatured);
                            setAdapterToListview();
                        } else {
                            errr_msg = getString(R.string.server_error);
                        }
                    }
                }
            });
            loadRadioList.execute(Constant.RADIO_BY_CITY_URL_1 + Constant.itemCity.getId() + Constant.RADIO_BY_CITY_URL_2 + "0");
        } else {
            errr_msg = getString(R.string.internet_not_connected);
            setEmpty();
        }
    }

    private void setAdapterToListview() {
        adapterCityDetails = new AdapterCityDetails(getActivity(), arrayList);
        recyclerView.setAdapter(adapterCityDetails);
        setEmpty();
    }

    public void setEmpty() {
        progressBar.setVisibility(View.GONE);
        if (arrayList.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ll_empty.setVisibility(View.GONE);
        } else {
            textView_empty.setText(errr_msg);
            recyclerView.setVisibility(View.GONE);
            ll_empty.setVisibility(View.VISIBLE);
        }
    }
}