package com.radiofmam.freebroadcasters;

import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.radiofmam.adapter.AdapterLanguage;
import com.radiofmam.interfaces.CityClickListener;
import com.radiofmam.item.ItemLanguage;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.JsonUtils;
import com.radiofmam.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;


public class FragmentLanguage extends Fragment {

    ArrayList<ItemLanguage> arrayList;
    RecyclerView recyclerView;
    public static AdapterLanguage adapterLanguage;
    LinearLayoutManager lLayout;
    SearchView searchView;
    JsonUtils jsonUtils;
    CircularProgressBar progressBar;
    int total_data = 0;
    Boolean isLoaded = false, isVisible = false;
    TextView textView_empty;
    public static AppCompatButton button_try;
    LinearLayout ll_empty;
    String errr_msg;
    SharedPref sharedPref;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_city, container, false);

        sharedPref = new SharedPref(getActivity());
        jsonUtils = new JsonUtils(getActivity());
        ll_empty = rootView.findViewById(R.id.ll_empty);
        textView_empty = rootView.findViewById(R.id.textView_empty_msg);
        button_try = rootView.findViewById(R.id.button_empty_try);
        ViewCompat.setBackgroundTintList(button_try, ColorStateList.valueOf(sharedPref.getFirstColor()));
        progressBar = rootView.findViewById(R.id.progressBar_cat);
        setHasOptionsMenu(true);

        arrayList = new ArrayList<>();

        adapterLanguage = new AdapterLanguage(getActivity(), arrayList, new CityClickListener() {
            @Override
            public void onClick() {
//                Intent intent = new Intent(getActivity(), FragmentLanguageDetails.class);
//                startActivity(intent);

               /* FragmentManager fm = FragmentLanguage.this.getParentFragment().getFragmentManager();
                FragmentLanguageDetails f1 = new FragmentLanguageDetails();
                FragmentTransaction ft = fm.beginTransaction();

                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.hide(FragmentLanguage.this.getParentFragment());
                ft.add(R.id.content_frame_activity, f1, Constant.itemLanguage.getName());
                ft.addToBackStack(Constant.itemLanguage.getName());
                ft.commit();
                ((BaseActivity)getActivity()).getSupportActionBar().setTitle(Constant.itemLanguage.getName());*/
            }
        });
        lLayout = new LinearLayoutManager(getActivity());
        recyclerView = rootView.findViewById(R.id.recyclerView_cat);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lLayout);

        if (isVisible && !isLoaded) {
            loadLanguage();
        }

        button_try.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadLanguage();
            }
        });

        return rootView;
    }

    private void loadLanguage() {
        if (JsonUtils.isNetworkAvailable(getActivity())) {
            new MyTask().execute(Constant.URL_LANGUAGE);
        } else {
            errr_msg = getString(R.string.internet_not_connected);
            setEmpty();
        }
        isLoaded = true;
    }

    private class MyTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            arrayList.clear();
            ll_empty.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            return JsonUtils.getJSONString(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            if (getActivity() != null) {
                super.onPostExecute(result);
                if (null == result || result.length() == 0) {
                    jsonUtils.showToast(getString(R.string.items_not_found));

                } else {

                    try {
                        JSONObject mainJson = new JSONObject(result);
                        JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);
                        JSONObject objJson;

                        total_data = jsonArray.length();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            objJson = jsonArray.getJSONObject(i);

                            String id = objJson.getString(Constant.LANG_ID);
                            String name = objJson.getString(Constant.LANG_NAME);
                            ItemLanguage objItem = new ItemLanguage(id, name, null);
                            arrayList.add(objItem);
                        }
                        errr_msg = getString(R.string.items_not_found);
                    } catch (Exception e) {
                        errr_msg = getString(R.string.server_error);
                        e.printStackTrace();
                    }

                    setAdapterToListview();
                }
            }
        }
    }

    public void setAdapterToListview() {
        recyclerView.setAdapter(adapterLanguage);
        setEmpty();
    }

    public void setEmpty() {
        progressBar.setVisibility(View.GONE);
        if (arrayList.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ll_empty.setVisibility(View.GONE);
        } else {
            textView_empty.setText(errr_msg);
            recyclerView.setVisibility(View.GONE);
            ll_empty.setVisibility(View.VISIBLE);
        }
    }

    public void showToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.search);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);

        searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(queryTextListener);
        super.onCreateOptionsMenu(menu, inflater);
    }

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            if (arrayList.size() > 0) {
                if (searchView.isIconified()) {
                    recyclerView.setAdapter(adapterLanguage);
                    adapterLanguage.notifyDataSetChanged();
                } else {
                    adapterLanguage.getFilter().filter(s);
                    adapterLanguage.notifyDataSetChanged();
                }
            }
            return true;
        }
    };

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        isVisible = isVisibleToUser;
        if (isVisibleToUser && isAdded() && !isLoaded) {
            loadLanguage();
        }
        super.setUserVisibleHint(isVisibleToUser);
    }
}
