package com.radiofmam.freebroadcasters;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.JsonUtils;

public class SplashActivity extends AppCompatActivity {

    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        showAd();
    }

    private void showAd(){
        JsonUtils jsonUtils = new JsonUtils(this);
        jsonUtils.setStatusColor(getWindow());

        try {
            Constant.isFromPush = getIntent().getExtras().getBoolean("ispushnoti", false);
        } catch (Exception e) {
            Constant.isFromPush = false;
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                final Intent i = new Intent(getApplicationContext(), BaseActivity.class);


                interstitialAd = new InterstitialAd(SplashActivity.this);

                interstitialAd.setAdUnitId(Constant.ad_inter_id);
                AdRequest adRequest = new AdRequest.Builder().build();

                interstitialAd.loadAd(adRequest);

                interstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {

                        if (interstitialAd.isLoaded()) {
                            interstitialAd.show();
                        }

                    }

                    @Override
                    public void onAdClosed() {
                        startActivity(i);
                        finish();

                        super.onAdClosed();
                    }


                    @Override
                    public void onAdOpened() {


                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        startActivity(i);
                        finish();
                    }
                });

            }
        }, 1000);
    }
}