package com.radiofmam.freebroadcasters;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.radiofmam.AsyncTasks.LoadRadioViewed;
import com.radiofmam.interfaces.RadioViewListener;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.JsonUtils;
import com.radiofmam.utils.ParserM3UToURL;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import co.mobiwise.library.radio.RadioListener;

public class PlayService extends Service implements RadioListener {

    static private final int NOTIFICATION_ID = 119;
    private String NOTIFICATION_CHANNEL_ID = "onlinradio_ch_1";
    static private PlayService service;
    static private Context context;
    static NotificationManager mNotificationManager;
    NotificationCompat.Builder mBuilder;
    static ItemRadio itemRadio;
    LoadSong loadSong;
    private Boolean isCanceled = false;
    public static final String ACTION_STOP = "com.prince.viavi.saveimage.action.STOP";
    public static final String ACTION_PLAY = "com.prince.viavi.saveimage.action.PLAY";
    public static final String ACTION_NEW = "com.prince.viavi.saveimage.action.NEW";

    static public void initialize(Context context, ItemRadio station) {
        PlayService.context = context;
        itemRadio = station;
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    static public void initNewContext(Context context) {
        PlayService.context = context;
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    static public PlayService getInstance() {
        if (service == null) {
            service = new PlayService();
        }
        return service;
    }

    @Override
    public void onCreate() {
        Constant.mRadioManager.registerListener(this);
        Constant.mRadioManager.setLogging(true);
        registerReceiver(onCallIncome, new IntentFilter("android.intent.action.PHONE_STATE"));

        Constant.mediaPlayer = new MediaPlayer();
        Constant.mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                if (!isCanceled) {
                    ((BaseActivity) context).setBuffer(false);
                    Constant.mediaPlayer.start();
                    createNotification();
                    changePlayPause(true);
                    ((BaseActivity) context).changeSongName(getString(R.string.unknown_song));
                } else {
                    isCanceled = false;
                    stopMediaPlayer();
                }
            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            switch (intent.getAction()) {
                case ACTION_STOP:
                    stop(intent);
                    break;
                case ACTION_PLAY:
                    loadSong = new LoadSong();
                    loadSong.execute();
                    break;
                case ACTION_NEW:
                    if (Constant.mRadioManager != null && Constant.mRadioManager.isPlaying()) {
                        Constant.mRadioManager.stopRadio();
                        Constant.mRadioManager.registerListener(PlayService.this);
                    } else if (Constant.mediaPlayer != null && Constant.mediaPlayer.isPlaying()) {
                        stopMediaPlayer();
                    }
                    loadSong = new LoadSong();
                    loadSong.execute();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return START_NOT_STICKY;
    }

    private class LoadSong extends AsyncTask<String, Void, Boolean> {

        protected void onPreExecute() {
            loadViewed(Constant.pos);
            ((BaseActivity) context).setBuffer(true);
            ((BaseActivity) context).changeSongName(getString(R.string.unknown_song));
        }

        protected Boolean doInBackground(final String... args) {
            try {
                String url = itemRadio.getRadiourl();
                if (url.endsWith("m3u8") || itemRadio.getType().equals("ondemand")) {
                    stopRadioManager();
                    Constant.mediaPlayer.reset();
                    Constant.mediaPlayer.setDataSource(url);
                    Constant.mediaPlayer.prepare();
                } else {
                    stopMediaPlayer();
                    if (url.endsWith(".m3u") || url.contains("yp.shoutcast.com/sbin/tunein-station.m3u?id=")) {
                        Constant.mRadioManager.startRadio(ParserM3UToURL.parse(url, "m3u"));
                    } else if (url.endsWith(".pls") || url.contains("listen.pls?sid=") || url.contains("yp.shoutcast.com/sbin/tunein-station.pls?id=")) {
                        Constant.mRadioManager.startRadio(ParserM3UToURL.parse(url, "pls"));
                    } else {
                        Constant.mRadioManager.startRadio(url);
                    }
                }
                return true;
            } catch (Exception e1) {
                e1.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (context != null) {
                super.onPostExecute(aBoolean);
                if (!aBoolean) {
                    ((BaseActivity) context).setBuffer(false);
                    Toast.makeText(context, getString(R.string.error_loading_radio), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onRadioLoading() {

    }

    @Override
    public void onRadioConnected() {
    }

    @Override
    public void onRadioStarted() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isCanceled) {
                    ((BaseActivity) context).setBuffer(false);
                    createNotification();
                    changePlayPause(true);
                } else {
                    isCanceled = false;
                    stopRadioManager();
                }
            }
        }, 0);
    }

    @Override
    public void onRadioStopped() {
    }

    @Override
    public void onMetaDataReceived(final String s, final String s1) {
        try {
            if (s != null && s.equals("StreamTitle")) {
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (s1.equals("")) {
                            ((BaseActivity) context).changeSongName(getString(R.string.unknown_song));
                        } else {
                            ((BaseActivity) context).changeSongName(s1);
                        }
                    }
                }, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                stopRadioManager();
                stopForeground(true);
                stopSelf();
                ((BaseActivity) context).setBuffer(false);
                ((BaseActivity) context).changePlayPause(false);
            }
        }, 0);
    }

    private void changePlayPause(Boolean play) {
        ((BaseActivity) context).changePlayPause(play);
    }

    public void stop(Intent intent) {
        if ((Constant.mRadioManager != null && Constant.mRadioManager.isPlaying()) || (Constant.mediaPlayer != null && Constant.mediaPlayer.isPlaying())) {
            unregisterReceiver(onCallIncome);
            changePlayPause(false);
            stopRadioManager();
            stopMediaPlayer();
            stopService(intent);
            stopForeground(true); // delete notification
        }
    }

    public void stopRadioManager() {
        if (Constant.mRadioManager != null && Constant.mRadioManager.isPlaying()) {
            Constant.mRadioManager.stopRadio();
            Constant.mRadioManager.unregisterListener(PlayService.this);
        }
    }

    public void stopMediaPlayer() {
        if (Constant.mediaPlayer != null && Constant.mediaPlayer.isPlaying()) {
            Constant.mediaPlayer.stop();
        }
    }


    private void createNotification() {
        Intent notificationIntent = new Intent(this, BaseActivity.class);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Intent stopIntent = new Intent(this, PlayService.class);
        stopIntent.setAction(ACTION_STOP);
        PendingIntent pstopIntent = PendingIntent.getService(this, 0,
                stopIntent, 0);


        mBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setTicker(itemRadio.getRadioName())
                .setContentTitle(itemRadio.getRadioName())
                .setContentText(itemRadio.getCityName())
                .setContentIntent(pi)
                .setSmallIcon(R.drawable.ic_radio)
                .setChannelId(NOTIFICATION_CHANNEL_ID)
                .setOnlyAlertOnce(true);

        NotificationChannel mChannel;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Online Channel";// The user-visible name of the channel.
            mChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(mChannel);

            MediaSessionCompat mMediaSession;
            mMediaSession = new MediaSessionCompat(context, "ONLINEMP3");
            mMediaSession.setFlags(
                    android.support.v4.media.session.MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                            MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);

            mBuilder.setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle()
                    .setMediaSession(mMediaSession.getSessionToken())
                    .setShowCancelButton(true)
                    .setShowActionsInCompactView(0)
                    .setCancelButtonIntent(
                            MediaButtonReceiver.buildMediaButtonPendingIntent(
                                    context, PlaybackStateCompat.ACTION_STOP)))
                    .addAction(new NotificationCompat.Action(
                            R.drawable.ic_cancelar, "Stop",
                            pstopIntent));
        } else {
            mBuilder.addAction(android.R.drawable.ic_delete, getString(R.string.stop), pstopIntent);
        }

        startForeground(NOTIFICATION_ID, mBuilder.build());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            new AsyncTask<String, String, String>() {

                @Override
                protected String doInBackground(String... strings) {
                    mBuilder.setLargeIcon(getBitmapFromURL(itemRadio.getRadioImageurl()));
                    return null;
                }

                @Override
                protected void onPostExecute(String s) {
                    mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
                    super.onPostExecute(s);
                }
            }.execute();
        }

    }

    private void loadViewed(final int pos) {
        if (JsonUtils.isNetworkAvailable(context)) {
            LoadRadioViewed loadRadioViewed = new LoadRadioViewed(new RadioViewListener() {
                @Override
                public void onEnd(String success) {
                }
            });
            loadRadioViewed.execute(Constant.RADIO_URL + Constant.arrayList_radio.get(pos).getRadioId());
        }

        else{
            Toast.makeText(context, getString(R.string.internet_not_connected), Toast.LENGTH_SHORT).show();
        }
    }

    public ItemRadio getPlayingRadioStation() {
        return itemRadio;
    }

    public static android.graphics.Bitmap getBitmapFromURL(String src) {
        try {
            java.net.URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            android.graphics.Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    BroadcastReceiver onCallIncome = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String a = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            if (Constant.isPlaying) {
                if (a.equals(TelephonyManager.EXTRA_STATE_OFFHOOK) || a.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    stopService(new Intent(context, PlayService.class));
                }
            }
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
