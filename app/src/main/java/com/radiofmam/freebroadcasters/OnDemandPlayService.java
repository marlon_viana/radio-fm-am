package com.radiofmam.freebroadcasters;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.radiofmam.AsyncTasks.LoadOnDemandViewed;
import com.radiofmam.interfaces.RadioViewListener;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.JsonUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import co.mobiwise.library.radio.RadioListener;

public class OnDemandPlayService extends Service implements RadioListener {

    static private final int NOTIFICATION_ID = 119;
    private String NOTIFICATION_CHANNEL_ID = "onlinradio_ch_1";
    static private OnDemandPlayService service;
    static private Context context;
    static NotificationManager mNotificationManager;
    NotificationCompat.Builder mBuilder;
    static ItemRadio itemRadio;
    LoadSong loadSong;
    private Boolean isCanceled = false;
    RemoteViews bigViews, smallViews;
    TrackSelector trackSelector;
    public static final String ACTION_STOP = "com.prince.viavi.saveimage.action.STOP";
    public static final String ACTION_PLAY = "com.prince.viavi.saveimage.action.PLAY";
    public static final String ACTION_FIRST_PLAY = "com.prince.viavi.saveimage.action.FIRSTPLAY";
    public static final String ACTION_PAUSE = "com.prince.viavi.saveimage.action.PAUSE";
    public static final String ACTION_PREVIOUS = "com.prince.viavi.saveimage.action.PREVIOUS";
    public static final String ACTION_NEXT = "com.prince.viavi.saveimage.action.NEXT";
    public static final String ACTION_NOTI_PLAY = "com.prince.viavi.saveimage.action.NOTI_PLAY";

    static public void initialize(Context context, ItemRadio station) {
        OnDemandPlayService.context = context;
        itemRadio = station;
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    static public void initNewContext(Context context) {
        OnDemandPlayService.context = context;
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    static public OnDemandPlayService getInstance() {
        if (service == null) {
            service = new OnDemandPlayService();
        }
        return service;
    }

    @Override
    public void onCreate() {
        registerReceiver(onCallIncome, new IntentFilter("android.intent.action.PHONE_STATE"));

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        Constant.exoPlayer_ondemand = ExoPlayerFactory.newSimpleInstance(context, trackSelector);
        Constant.exoPlayer_ondemand.addListener(listener);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            switch (intent.getAction()) {
                case ACTION_STOP:
                    stop(intent);
                    break;
                case ACTION_PLAY:
                    play();
                    break;
                case ACTION_PAUSE:
                    pause();
                    break;
                case ACTION_NOTI_PLAY:
                    if (Constant.isPlaying) {
                        pause();
                    } else {
                        if (JsonUtils.isNetworkAvailable(context)) {
                            play();
                        } else {
                            Toast.makeText(context, getResources().getString(R.string.internet_not_connected), Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case ACTION_PREVIOUS:
                    if (JsonUtils.isNetworkAvailable(context)) {
                        previous();
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.internet_not_connected), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case ACTION_NEXT:
                    if (JsonUtils.isNetworkAvailable(context)) {
                        next();
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.internet_not_connected), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case ACTION_FIRST_PLAY:
                    firstplay();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return START_NOT_STICKY;
    }

    private class LoadSong extends AsyncTask<String, Void, Boolean> {

        protected void onPreExecute() {
            ((BaseActivity) context).setBuffer(true);
            Constant.isPlayed = true;
            loadViewed(Constant.pos);
        }

        protected Boolean doInBackground(final String... args) {
            try {
                String url = itemRadio.getRadiourl();

                DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context,
                        Util.getUserAgent(context, "onlineradio"), bandwidthMeter);
                ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
                MediaSource videoSource = new ExtractorMediaSource(Uri.parse(url),
                        dataSourceFactory, extractorsFactory, null, null);
                Constant.exoPlayer_ondemand.prepare(videoSource);
                Constant.exoPlayer_ondemand.setPlayWhenReady(true);

                return true;
            } catch (Exception e1) {
                e1.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (context != null) {
                super.onPostExecute(aBoolean);
                if (!aBoolean) {
                    ((BaseActivity) context).setBuffer(false);
                }
            }
        }
    }

    @Override
    public void onRadioLoading() {

    }

    @Override
    public void onRadioConnected() {
        Log.e("radio", "connected");
    }

    @Override
    public void onRadioStarted() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isCanceled) {
                    ((BaseActivity) context).setBuffer(false);
                    createNotification();
                    changePlayPause(true);
                } else {
                    isCanceled = false;
                }
            }
        }, 0);
    }

    @Override
    public void onRadioStopped() {
    }

    @Override
    public void onMetaDataReceived(final String s, final String s1) {
        try {
            if (s != null && s.equals("StreamTitle")) {
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (s1.equals("")) {
                            ((BaseActivity) context).changeSongName(getString(R.string.unknown_song));
                        } else {
                            ((BaseActivity) context).changeSongName(s1);
                        }
                    }
                }, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError() {
        ((BaseActivity) context).setBuffer(false);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                changePlayPause(false);
            }
        }, 0);
    }

    private void changePlayPause(Boolean play) {
        ((BaseActivity) context).changePlayPause(play);
    }

    private void next() {
        if (Constant.pos != Constant.arrayList_radio.size() - 1) {
            Constant.pos = Constant.pos + 1;
        } else {
            Constant.pos = 0;
        }
        itemRadio = Constant.arrayList_radio.get(Constant.pos);
        firstplay();
    }

    private void previous() {
        if (Constant.pos != 0) {
            Constant.pos = Constant.pos - 1;
        } else {
            Constant.pos = Constant.arrayList_radio.size() - 1;
        }
        itemRadio = Constant.arrayList_radio.get(Constant.pos);
        firstplay();
    }

    private void pause() {
        Constant.exoPlayer_ondemand.setPlayWhenReady(false);
        changePlayPause(false);
        updateNotiPlay(false);
    }

    private void play() {
        Constant.exoPlayer_ondemand.setPlayWhenReady(true);
        changePlayPause(true);
        updateNotiPlay(true);
        ((BaseActivity) context).seekUpdation();
    }

    private void firstplay() {
        loadSong = new LoadSong();
        loadSong.execute();
    }

    public void stop(Intent intent) {
        if (Constant.exoPlayer_ondemand != null) {
            unregisterReceiver(onCallIncome);
            changePlayPause(false);
            stopMediaPlayer();
            stopService(intent);
            stopForeground(true); // delete notification
        }
    }

    public void stopMediaPlayer() {
        if (Constant.exoPlayer_ondemand != null && Constant.exoPlayer_ondemand.getPlayWhenReady()) {
            Constant.exoPlayer_ondemand.stop();
            Constant.exoPlayer_ondemand.release();
        }
    }

    private void createNotification() {
        Intent notificationIntent = new Intent(this, BaseActivity.class);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent previousIntent = new Intent(this, OnDemandPlayService.class);
        previousIntent.setAction(ACTION_PREVIOUS);
        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0,
                previousIntent, 0);

        Intent playIntent = new Intent(this, OnDemandPlayService.class);
        playIntent.setAction(ACTION_NOTI_PLAY);
        PendingIntent pplayIntent = PendingIntent.getService(this, 0,
                playIntent, 0);

        Intent nextIntent = new Intent(this, OnDemandPlayService.class);
        nextIntent.setAction(ACTION_NEXT);
        PendingIntent pnextIntent = PendingIntent.getService(this, 0,
                nextIntent, 0);

        Intent closeIntent = new Intent(this, OnDemandPlayService.class);
        closeIntent.setAction(ACTION_STOP);
        PendingIntent pcloseIntent = PendingIntent.getService(this, 0,
                closeIntent, PendingIntent.FLAG_CANCEL_CURRENT);


        mBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setTicker(itemRadio.getRadioName())
                .setContentTitle(itemRadio.getRadioName())
                .setContentText(itemRadio.getCityName())
                .setContentIntent(pi)
                .setSmallIcon(R.drawable.ic_radio)
                .setChannelId(NOTIFICATION_CHANNEL_ID)
                .setOnlyAlertOnce(true);

        NotificationChannel mChannel;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Online Channel";// The user-visible name of the channel.
            mChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(mChannel);

            MediaSessionCompat mMediaSession;
            mMediaSession = new MediaSessionCompat(context, "ONLINEMP3");
            mMediaSession.setFlags(
                    MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                            MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);

            mBuilder.setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle()
                    .setMediaSession(mMediaSession.getSessionToken())
                    .setShowCancelButton(true)
                    .setShowActionsInCompactView(0, 1, 2)
                    .setCancelButtonIntent(
                            MediaButtonReceiver.buildMediaButtonPendingIntent(
                                    context, PlaybackStateCompat.ACTION_STOP)))
                    .addAction(new NotificationCompat.Action(
                            R.mipmap.ic_noti_previous, "Previous",
                            ppreviousIntent))
                    .addAction(new NotificationCompat.Action(
                            R.drawable.ic_pausa, "Pause",
                            pplayIntent))
                    .addAction(new NotificationCompat.Action(
                            R.mipmap.ic_noti_next, "Next",
                            pnextIntent))
                    .addAction(new NotificationCompat.Action(
                            R.drawable.ic_cancelar, "Close",
                            pcloseIntent));
        } else {
            bigViews = new RemoteViews(getPackageName(), R.layout.layout_notification);
            smallViews = new RemoteViews(getPackageName(), R.layout.layout_noti_small);
            bigViews.setOnClickPendingIntent(R.id.imageView_noti_play, pplayIntent);

            bigViews.setOnClickPendingIntent(R.id.imageView_noti_next, pnextIntent);

            bigViews.setOnClickPendingIntent(R.id.imageView_noti_prev, ppreviousIntent);

            bigViews.setOnClickPendingIntent(R.id.imageView_noti_close, pcloseIntent);
            smallViews.setOnClickPendingIntent(R.id.status_bar_collapse, pcloseIntent);

            bigViews.setImageViewResource(R.id.imageView_noti_play, android.R.drawable.ic_media_pause);

            bigViews.setTextViewText(R.id.textView_noti_name, Constant.arrayList_radio.get(Constant.pos).getRadioName());
            smallViews.setTextViewText(R.id.status_bar_track_name, Constant.arrayList_radio.get(Constant.pos).getRadioName());

            bigViews.setImageViewResource(R.id.imageView_noti, R.mipmap.ic_launcher);
            smallViews.setImageViewResource(R.id.status_bar_album_art, R.mipmap.ic_launcher);

            mBuilder.setCustomContentView(smallViews)
                    .setCustomBigContentView(bigViews);
        }

        startForeground(NOTIFICATION_ID, mBuilder.build());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            new AsyncTask<String, String, String>() {

                @Override
                protected String doInBackground(String... strings) {
                    mBuilder.setLargeIcon(getBitmapFromURL(itemRadio.getRadioImageurl()));
                    return null;
                }

                @Override
                protected void onPostExecute(String s) {
                    mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
                    super.onPostExecute(s);
                }
            }.execute();
        }
    }

    private void loadViewed(final int pos) {
        if (JsonUtils.isNetworkAvailable(context)) {
            LoadOnDemandViewed loadOnDemandViewed = new LoadOnDemandViewed(new RadioViewListener() {
                @Override
                public void onEnd(String success) {
                }
            });
            loadOnDemandViewed.execute(Constant.ON_DEMAND_SINGLE_URL + Constant.arrayList_radio.get(pos).getRadioId());
        }
    }

    private void updateNotiPlay(Boolean isPlay) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mBuilder.mActions.remove(1);
            Intent playIntent = new Intent(this, OnDemandPlayService.class);
            playIntent.setAction(ACTION_NOTI_PLAY);
            PendingIntent ppreviousIntent = PendingIntent.getService(this, 0, playIntent, 0);
            if (isPlay) {
                mBuilder.mActions.add(1, new NotificationCompat.Action(
                        R.drawable.ic_pausa, "Pause",
                        ppreviousIntent));

            } else {
                mBuilder.mActions.add(1, new NotificationCompat.Action(
                        R.drawable.ic_play, "Play",
                        ppreviousIntent));
            }
        } else {
            if (isPlay) {
                bigViews.setImageViewResource(R.id.imageView_noti_play, android.R.drawable.ic_media_pause);
            } else {
                bigViews.setImageViewResource(R.id.imageView_noti_play, android.R.drawable.ic_media_play);
            }
        }
        mNotificationManager.notify(119, mBuilder.build());
    }

    public ItemRadio getPlayingRadioStation() {
        return itemRadio;
    }

    public static android.graphics.Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            android.graphics.Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    BroadcastReceiver onCallIncome = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String a = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            if (Constant.isPlaying) {
                if (a.equals(TelephonyManager.EXTRA_STATE_OFFHOOK) || a.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    stopService(new Intent(context, OnDemandPlayService.class));
                }
            }
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    Player.EventListener listener = new Player.EventListener() {

        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            if (playbackState == Player.STATE_ENDED) {
                onCompletion();
            }
            if (playbackState == Player.STATE_READY && playWhenReady) {
                if (!isCanceled) {
                    createNotification();
                    changePlayPause(true);
                    ((BaseActivity) context).seekUpdation();
                    ((BaseActivity) context).setBuffer(false);

                } else {
                    isCanceled = false;
                    stopMediaPlayer();
                }
            }
        }

        @Override
        public void onRepeatModeChanged(int repeatMode) {

        }

        @Override
        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
        }

        @Override
        public void onPositionDiscontinuity(int reason) {

        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

        }

        @Override
        public void onSeekProcessed() {

        }
    };

    public void onCompletion() {
        next();
    }
}
