package com.radiofmam.freebroadcasters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.radiofmam.AsyncTasks.LoadAbout;
import com.radiofmam.interfaces.AboutListener;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.DBHelper;
import com.radiofmam.utils.JsonUtils;
import com.radiofmam.utils.StatusBarView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Activity_About extends AppCompatActivity {

    Toolbar toolbar;
    StatusBarView statusBarView;
    WebView webView;
    DBHelper dbHelper;
    TextView textView_appname, textView_email, textView_website, textView_company, textView_contact, textView_version;
    ImageView imageView_logo;
    String website, email, desc, applogo, appname, appversion, appauthor, appcontact, privacy, developedby;
    ProgressDialog pbar;
    LoadAbout loadAbout;
    JsonUtils jsonUtils;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        dbHelper = new DBHelper(this);
        jsonUtils = new JsonUtils(this);
        jsonUtils.forceRTLIfSupported(getWindow());
        jsonUtils.setStatusColor(getWindow());

        statusBarView = findViewById(R.id.statusBar_about);
        toolbar = this.findViewById(R.id.toolbar_about);
        toolbar.setTitle(getString(R.string.menu_about));
        toolbar.setBackground(jsonUtils.getGradientDrawableToolbar());
        statusBarView.setBackground(jsonUtils.getGradientDrawableToolbar());
        this.setSupportActionBar(toolbar);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        pbar = new ProgressDialog(this);
        pbar.setMessage(getString(R.string.app_name));
        pbar.setCancelable(false);

        webView = findViewById(R.id.webView);
        textView_appname = findViewById(R.id.textView_about_appname);
        textView_email = findViewById(R.id.textView_about_email);
        textView_website = findViewById(R.id.textView_about_site);
        textView_company = findViewById(R.id.textView_about_company);
        textView_contact = findViewById(R.id.textView_about_contact);
        textView_version = findViewById(R.id.textView_about_appversion);
        imageView_logo = findViewById(R.id.imageView_about_logo);

        setTexts();
        setVariables();

        dbHelper.getAbout();


        /*if (JsonUtils.isNetworkAvailable(Activity_About.this)) {

        } else {
            if (dbHelper.getAbout()) {
                setTexts();
                setVariables();
            }
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
        return true;
    }

    private void setTexts() {
        appname = Constant.itemAbout.getAppName();
        applogo = Constant.itemAbout.getAppLogo();
        desc = Constant.itemAbout.getAppDesc();
        appversion = Constant.itemAbout.getAppVersion();
        appauthor = Constant.itemAbout.getAuthor();
        appcontact = Constant.itemAbout.getContact();
        email = Constant.itemAbout.getEmail();
        website = Constant.itemAbout.getWebsite();
        privacy = Constant.itemAbout.getPrivacy();
        developedby = Constant.itemAbout.getDevelopedby();
    }

    public void setVariables() {
        textView_appname.setText(appname);
        if (!email.trim().isEmpty()) {
            textView_email.setText(email);
        }

        if (!website.trim().isEmpty()) {
            textView_website.setText(website);
        }

        if (!appauthor.trim().isEmpty()) {
            textView_company.setText(appauthor);
        }

        if (!appcontact.trim().isEmpty()) {

            textView_contact.setText(appcontact);
        }

        if (!appversion.trim().isEmpty()) {
            textView_version.setText(appversion);
        }

        if (applogo.trim().isEmpty()) {
            imageView_logo.setVisibility(View.GONE);
        } else {
            Picasso
                    .get()
                    .load(Constant.URL_ABOUT_US_LOGO + applogo)
                    .into(imageView_logo);
        }

        String mimeType = "text/html;charset=UTF-8";
        String encoding = "utf-8";

        String text = "<html><head>"
                + "<style> body{color:#cc000000 !important;text-align:left}"
                + "</style></head>"
                + "<body>"
                + desc
                + "</body></html>";


        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.loadData(text, mimeType, encoding);
    }
}
