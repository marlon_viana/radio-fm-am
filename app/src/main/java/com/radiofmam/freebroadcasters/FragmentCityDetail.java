package com.radiofmam.freebroadcasters;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.radiofmam.AsyncTasks.LoadHome;
import com.radiofmam.AsyncTasks.LoadRadioList;
import com.radiofmam.adapter.AdapterCountriesList;
import com.radiofmam.interfaces.HomeListener;
import com.radiofmam.interfaces.RadioListListener;
import com.radiofmam.item.ItemCity;
import com.radiofmam.item.ItemOnDemandCat;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.DBHelper;
import com.radiofmam.utils.JsonUtils;
import com.radiofmam.utils.SharedPref;

import java.util.ArrayList;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

@SuppressLint("ValidFragment")
public class FragmentCityDetail extends Fragment implements BaseSliderView.OnSliderClickListener {

    DBHelper dbHelper;
    JsonUtils jsonUtils;
    SliderLayout sliderLayout;
    ArrayList<ItemRadio> arrayList_radio_latest, arrayList_radio_featured, arrayList_radio_mostviewed;
    ArrayList<ItemOnDemandCat> arrayList_ondemandcat;
    public static AdapterCountriesList adapterRadioList, adapterRadioList_mostview, adapterRadioList_featured;
    LoadRadioList loadRadioList;
    LoadHome loadHome;
    CircularProgressBar progressBar;
    NestedScrollView scrollView;
    RecyclerView recyclerView, recyclerView_mostview, recyclerView_featured;
    Boolean isLoaded = false, isVisible = false;
    SearchView searchView;
    LinearLayout topTitle,latestTitle;
    TextView textView_empty,toolbarTitle;
    public static AppCompatButton button_try;
    LinearLayout ll_empty;
    SharedPref sharedPref;
    String errr_msg;
    Toolbar toolbar;
    DrawerLayout drawer;
    TabLayout tabLayout;

    private String contryName;

    @SuppressLint("ValidFragment")
    public FragmentCityDetail(String contry) {
        this.contryName = contry;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_radio, container, false);

        sharedPref = new SharedPref(getActivity());
        ll_empty = rootView.findViewById(R.id.ll_empty);
        textView_empty = rootView.findViewById(R.id.textView_empty_msg);
        button_try = rootView.findViewById(R.id.button_empty_try);
        ViewCompat.setBackgroundTintList(button_try, ColorStateList.valueOf(sharedPref.getFirstColor()));

        tabLayout= getActivity().findViewById(R.id.tabs_home);
        tabLayout.setVisibility(View.GONE);

        toolbar = ((BaseActivity) getActivity()).toolbar;
        ((BaseActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarTitle= getActivity().findViewById(R.id.text_toolbar);

        try {
            toolbarTitle.setText(contryName);
        }catch (Exception e){
            Log.e("ERROR TOOL",e.toString());
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tabLayout.setVisibility(View.VISIBLE);
                    try {
                        toolbarTitle.setText(getString(R.string.countrys));
                    }catch (Exception e){

                    }

                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag("CITY_DETAIL_FRAGMENT_TAG");
                    if(fragment != null){
                        getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                        getActivity().onBackPressed();
                    }

                }
            });
        }


        dbHelper = new DBHelper(getActivity());
        jsonUtils = new JsonUtils(getActivity());

        arrayList_radio_latest = new ArrayList<>();
        arrayList_radio_mostviewed = new ArrayList<>();
        arrayList_radio_featured = new ArrayList<>();
        arrayList_ondemandcat = new ArrayList<>();

        topTitle = rootView.findViewById(R.id.topTitle);
        latestTitle = rootView.findViewById(R.id.latestTitle);


        progressBar = rootView.findViewById(R.id.progressBar_home);
        scrollView = rootView.findViewById(R.id.scrollView_home);

        sliderLayout = rootView.findViewById(R.id.sliderLayout);

        recyclerView = rootView.findViewById(R.id.recyclerView_radiolist);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        recyclerView_mostview = rootView.findViewById(R.id.recyclerView_mostview);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        recyclerView_mostview.setLayoutManager(llm);
        recyclerView_mostview.setHasFixedSize(true);

        recyclerView_featured = rootView.findViewById(R.id.recyclerView_featured);
        LinearLayoutManager llm_featured = new LinearLayoutManager(getActivity());
        recyclerView_featured.setLayoutManager(llm_featured);
        recyclerView_featured.setHasFixedSize(true);

        recyclerView.setNestedScrollingEnabled(false);
        recyclerView_mostview.setNestedScrollingEnabled(false);
        recyclerView_featured.setNestedScrollingEnabled(false);

        if (!isLoaded) {
            loadList();
            isLoaded = true;
        }


        button_try.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadList();
            }
        });

        setHasOptionsMenu(true);
        return rootView;
    }

    private void loadList() {
        if (JsonUtils.isNetworkAvailable(getActivity())) {
            loadRadioList = new LoadRadioList(new RadioListListener() {
                @Override
                public void onStart() {
                    if (getActivity() != null) {
                        recyclerView.setVisibility(View.VISIBLE);
                        ll_empty.setVisibility(View.GONE);
                        arrayList_radio_latest.clear();
                        progressBar.setVisibility(View.VISIBLE);
                        scrollView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onEnd(String success, ArrayList<ItemRadio> arrayList) {
                    if (getActivity() != null) {
                        arrayList_radio_latest.addAll(arrayList);

                        if (arrayList.size() > 0) {
                            adapterRadioList = new AdapterCountriesList(getActivity(), arrayList_radio_latest);
                            recyclerView.setAdapter(adapterRadioList);
                            if (Constant.arrayList_radio.size() == 0) {
                                Constant.arrayList_radio.addAll(arrayList_radio_latest);
                                ((BaseActivity) getActivity()).changeText(Constant.arrayList_radio.get(0));
                            }

                            latestTitle.setVisibility(View.VISIBLE);

                        }
                        loadHome();
                    }
                }
            });

            String countryName = contryName;
            if(countryName==null){countryName="";}
            loadRadioList.execute(Constant.RADIOLISTlATEST_URL+"&country_name="+countryName+"");

        } else {
            errr_msg = getString(R.string.internet_not_connected);
            setEmpty();
            jsonUtils.showToast(getString(R.string.internet_not_connected));
        }
    }

    private void loadHome() {
        loadHome = new LoadHome(new HomeListener() {
            @Override
            public void onStart() {
                if (getActivity() != null) {
                    arrayList_radio_mostviewed.clear();
                    arrayList_radio_featured.clear();
                }
            }

            @Override
            public void onEnd(String success, ArrayList<ItemRadio> arrayList_featured, ArrayList<ItemRadio> arrayList_mostviewed, ArrayList<ItemOnDemandCat> arrayList_ondemand_cat) {
                if (getActivity() != null) {
                    arrayList_radio_featured.addAll(arrayList_featured);
                    arrayList_radio_mostviewed.addAll(arrayList_mostviewed);
                    arrayList_ondemandcat.addAll(arrayList_ondemand_cat);

                    progressBar.setVisibility(View.GONE);
                    scrollView.setVisibility(View.VISIBLE);

                    if (arrayList_featured.size() > 0) {
                        topTitle.setVisibility(View.VISIBLE);
                    }

                    if (arrayList_mostviewed.size() > 0) {
                        adapterRadioList_mostview = new AdapterCountriesList(getActivity(), arrayList_radio_mostviewed);
                        recyclerView_mostview.setAdapter(adapterRadioList_mostview);
                        adapterRadioList_featured = new AdapterCountriesList(getActivity(), arrayList_radio_featured);
                        recyclerView_featured.setAdapter(adapterRadioList_featured);
                        loadSlider();


                    }
                }
            }
        });

        String countryName = contryName;
        if(countryName==null){countryName="";}
        loadHome.execute(Constant.URL_HOME+"&country_name="+countryName+"");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.search);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);

        searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(queryTextListener);
        super.onCreateOptionsMenu(menu, inflater);
    }

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            if (arrayList_radio_latest.size() > 0) {
                if (searchView.isIconified()) {
                    recyclerView.setAdapter(adapterRadioList);
                    adapterRadioList.notifyDataSetChanged();
                    recyclerView_featured.setVisibility(View.VISIBLE);
                    topTitle.setVisibility(View.VISIBLE);
                } else {
                    recyclerView_featured.setVisibility(View.GONE);
                    topTitle.setVisibility(View.GONE);
                    adapterRadioList.getFilter().filter(s);
                    adapterRadioList.notifyDataSetChanged();
                }
            }
            return true;
        }
    };

    private void loadSlider() {
        for (int i = 0; i < arrayList_ondemandcat.size(); i++) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            textSliderView.frequency("(" + arrayList_ondemandcat.get(i).getTotalItems() + ")")
                    .language("")
                    .name(arrayList_ondemandcat.get(i).getName())
                    .image(arrayList_ondemandcat.get(i).getImage())
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putInt("pos", i);

            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Default);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Right_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(5000);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
       /* FragmentManager fm = FragmentCityDetail.this.getParentFragment().getFragmentManager();
        FragmentOnDemandDetails f1 = new FragmentOnDemandDetails();
        FragmentTransaction ft = fm.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putSerializable("item", arrayList_ondemandcat.get(slider.getBundle().getInt("pos")));
        f1.setArguments(bundle);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.hide(FragmentCityDetail.this.getParentFragment());
        ft.add(R.id.content_frame_activity, f1, arrayList_ondemandcat.get(slider.getBundle().getInt("pos")).getName());
        ft.addToBackStack(arrayList_ondemandcat.get(slider.getBundle().getInt("pos")).getName());
        ft.commit();
        ((BaseActivity)getActivity()).getSupportActionBar().setTitle(arrayList_ondemandcat.get(slider.getBundle().getInt("pos")).getName());*/
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        isVisible = isVisibleToUser;
        if (isVisibleToUser && isAdded() && !isLoaded) {
            loadList();
            isLoaded = true;
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    public void setEmpty() {
        progressBar.setVisibility(View.GONE);
        if (arrayList_radio_latest.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ll_empty.setVisibility(View.GONE);
        } else {
            textView_empty.setText(errr_msg);
            recyclerView.setVisibility(View.GONE);
            ll_empty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        drawer = ((BaseActivity) getActivity()).drawer;
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }
}
