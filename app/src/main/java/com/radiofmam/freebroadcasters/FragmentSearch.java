package com.radiofmam.freebroadcasters;

import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.radiofmam.adapter.AdapterCityDetails;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.DBHelper;
import com.radiofmam.utils.JsonUtils;
import com.radiofmam.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

public class FragmentSearch extends Fragment {

    DBHelper dbHelper;
    RecyclerView recyclerView;
    AdapterCityDetails adapter;
    GridLayoutManager lLayout;
    SearchView searchView;
    ArrayList<ItemRadio> arraylist;
    CircularProgressBar progressBar;
    TextView textView_empty;
    public static AppCompatButton button_try;
    LinearLayout ll_empty;
    String errr_msg;
    SharedPref sharedPref;
    JsonUtils jsonUtils;
    Toolbar toolbar;
    DrawerLayout drawer;

    TabLayout tabLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_city_detail, container, false);


        toolbar = ((BaseActivity) getActivity()).toolbar;
        tabLayout= getActivity().findViewById(R.id.tabs_home);
        tabLayout.setVisibility(View.GONE);

        ((BaseActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tabLayout.setVisibility(View.VISIBLE);
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag("SEARCH_FRAGMENT_TAG");
                    if(fragment != null){
                        getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                        getActivity().onBackPressed();
                    }
                }
            });
        }

        dbHelper = new DBHelper(getActivity());
        sharedPref = new SharedPref(getActivity());
        jsonUtils = new JsonUtils(getActivity());

        arraylist = new ArrayList<>();
        progressBar = rootView.findViewById(R.id.progressBar_city_details);

        ll_empty = rootView.findViewById(R.id.ll_empty);
        textView_empty = rootView.findViewById(R.id.textView_empty_msg);
        button_try = rootView.findViewById(R.id.button_empty_try);
        ViewCompat.setBackgroundTintList(button_try, ColorStateList.valueOf(sharedPref.getFirstColor()));

        lLayout = new GridLayoutManager(getActivity(), 1);

        recyclerView = rootView.findViewById(R.id.recyclerView_city_detail);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lLayout);

        loadRadioSearch();

        button_try.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadRadioSearch();
            }
        });

        setHasOptionsMenu(true);
        return rootView;
    }

    private void loadRadioSearch() {
        if (JsonUtils.isNetworkAvailable(getActivity())) {
            new MyTask().execute(Constant.URL_SEARCH + Constant.search_text);
        } else {
            errr_msg = getString(R.string.internet_not_connected);
            setEmpty();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.search);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);

        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(queryTextListener);
        super.onCreateOptionsMenu(menu, inflater);
    }

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            if (!s.trim().equals("")) {
                Constant.search_text = s.replace(" ", "%20");
                loadRadioSearch();
            }
            return true;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            return false;
        }
    };

    private class MyTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            arraylist.clear();
            ll_empty.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            return JsonUtils.getJSONString(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(getActivity()!= null) {
                if (null == result || result.length() == 0) {
                    jsonUtils.showToast(getString(R.string.items_not_found));
                    errr_msg = getString(R.string.items_not_found);
                } else {
                    try {
                        JSONObject mainJson = new JSONObject(result);
                        JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);
                        JSONObject objJson;

                        for (int i = 0; i < jsonArray.length(); i++) {
                            objJson = jsonArray.getJSONObject(i);

                            String id = objJson.getString(Constant.RADIO_ID);
                            String name = objJson.getString(Constant.RADIO_NAME);
                            String url = objJson.getString(Constant.RADIO_IMAGE);
                            String freq = objJson.getString(Constant.RADIO_FREQ);
                            String img = objJson.getString(Constant.RADIO_IMAGE_BIG);
                            String views = objJson.getString(Constant.RADIO_VIEWS);
                            String lang_name = objJson.getString(Constant.LANG_NAME);
                            String description = objJson.getString(Constant.RADIO_DESC);
                            ItemRadio objItem = new ItemRadio(id, name, url, freq, img, views, "", "", lang_name, description, "radio");
                            arraylist.add(objItem);
                        }
                        errr_msg = getString(R.string.items_not_found);
                    } catch (Exception e) {
                        errr_msg = getString(R.string.server_error);
                        e.printStackTrace();
                    }
                    setAdapterToListview();
                }
            }
        }
    }

    public void setAdapterToListview() {
        adapter = new AdapterCityDetails(getActivity(), arraylist);
        recyclerView.setAdapter(adapter);
        setEmpty();
    }

    public void setEmpty() {
        progressBar.setVisibility(View.GONE);
        if (arraylist.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ll_empty.setVisibility(View.GONE);
        } else {
            textView_empty.setText(errr_msg);
            recyclerView.setVisibility(View.GONE);
            ll_empty.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();

        ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        drawer = ((BaseActivity) getActivity()).drawer;
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

}