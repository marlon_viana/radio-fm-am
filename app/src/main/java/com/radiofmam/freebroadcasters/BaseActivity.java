package com.radiofmam.freebroadcasters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;
import com.labo.kaji.relativepopupwindow.RelativePopupWindow;
import com.makeramen.roundedimageview.RoundedImageView;
import com.pkmmte.view.CircularImageView;
import com.radiofmam.utils.ViewPageAdapter;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;
import com.radiofmam.AsyncTasks.LoadAbout;
import com.radiofmam.AsyncTasks.LoadRadioViewed;
import com.radiofmam.interfaces.AboutListener;
import com.radiofmam.interfaces.AdConsentListener;
import com.radiofmam.interfaces.InterAdListener;
import com.radiofmam.interfaces.RadioViewListener;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.utils.AdConsent;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.DBHelper;
import com.radiofmam.utils.JsonUtils;
import com.radiofmam.utils.SharedPref;
import com.radiofmam.utils.StatusBarView;

import co.mobiwise.library.radio.RadioListener;
import co.mobiwise.library.radio.RadioManager;
import dyanamitechetan.vusikview.VusikView;
import fr.castorflex.android.circularprogressbar.CircularProgressBar;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,NumberPicker.OnValueChangeListener,View.OnClickListener {

    Toolbar toolbar;
    StatusBarView statusBarView;
    FragmentManager fm;

    NavigationView navigationView;
    SlidingUpPanelLayout slidingPanel;
    BottomSheetDialog dialog_desc;
    DBHelper dbHelper;
    AdConsent adConsent;
    ProgressDialog progressDialog;
    CircularProgressBar circularProgressBar, circularProgressBar_collapse;
    SeekBar seekbar_song;
    LinearLayout ll_ad;
    ConstraintLayout ll_player_expand,rl_collapse, ll_play_collapse;
    RelativeLayout rl_expand, rl_song_seekbar;
    CircularImageView imageView_player;
    RoundedImageView imageView_radio;
    ImageView imageView_play, imageView_share, imageView_next, imageView_previous, imageView_next_expand, imageView_previous_expand, imageView_fav, imageView_collapse, imageView_desc, imageView_volume;
    FloatingActionButton fab_play_expand;
    TextView textView_name, textView_song, textView_freq_expand, textView_radio_expand, textView_radio_expand2, textView_song_expand, textView_song_duration, textView_total_duration,
            textViewToolbar;
    JsonUtils jsonUtils;
    LoadAbout loadAbout;
    DrawerLayout drawer;
    Boolean isExpand = false, isLoaded = false;
    SharedPref sharedPref;
    final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 102;
    private Handler seekHandler = new Handler();
    View view_lollipop;
    VusikView vusikView;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabsIconos ={R.drawable.ic_tierra,R.drawable.ic_radio,R.drawable.ic_fav};

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        addView();
        checkPer();

        gestorTab();

        progressDialog = new ProgressDialog(BaseActivity.this);
        progressDialog.setMessage(getString(R.string.loading));
        sharedPref = new SharedPref(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view_lollipop.setVisibility(View.VISIBLE);
        }

        Constant.mRadioManager = RadioManager.with(getApplicationContext());
        Constant.mRadioManager.connect();
        Constant.mRadioManager.registerListener(new RadioListener() {
            @Override
            public void onRadioLoading() {
            }

            @Override
            public void onRadioConnected() {
                if (Constant.isFromPush) {
                    Constant.isFromPush = false;
                    clickRadioStation(0);
                }
            }

            @Override
            public void onRadioStarted() {
            }

            @Override
            public void onRadioStopped() {
            }

            @Override
            public void onMetaDataReceived(String s, String s2) {
            }

            @Override
            public void onError() {
            }
        });

        statusBarView = findViewById(R.id.statusBar);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        dbHelper = new DBHelper(this);
        jsonUtils = new JsonUtils(this, interAdListener);
        jsonUtils.forceRTLIfSupported(getWindow());
        jsonUtils.setStatusColor(getWindow());

        fm = getSupportFragmentManager();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        slidingPanel = findViewById(R.id.sliding_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        setIfPlaying();

        adConsent = new AdConsent(this, new AdConsentListener() {
            @Override
            public void onConsentUpdate() {
                jsonUtils.showBannerAd(ll_ad);
                jsonUtils.loadInter();
            }
        });

        if (JsonUtils.isNetworkAvailable(this)) {
            loadAboutData();
        } else {
            adConsent.checkForConsent();
            dbHelper.getAbout();
            jsonUtils.showToast(getString(R.string.internet_not_connected));
        }

        seekbar_song.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                try {
                    Constant.exoPlayer_ondemand.seekTo((int) JsonUtils.getSeekFromPercentage(progress, JsonUtils.calculateTime(Constant.arrayList_radio.get(Constant.pos).getDuration())));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        rl_collapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingPanel.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });

        rl_expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        slidingPanel.setDragView(rl_collapse);
        slidingPanel.setShadowHeight(0);
        slidingPanel.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                if (slideOffset == 0.0f) {
                    isExpand = false;
                    rl_expand.setVisibility(View.GONE);
                } else if (slideOffset > 0.0f && slideOffset < 1.0f) {
                    if (isExpand) {

                        rl_collapse.setVisibility(View.VISIBLE);
                        rl_expand.setAlpha(slideOffset);
                        rl_collapse.setAlpha(1.0f - slideOffset);
                    } else {

                        rl_expand.setVisibility(View.VISIBLE);
                        rl_expand.setAlpha(0.0f + slideOffset);
                        rl_collapse.setAlpha(1.0f - slideOffset);
                    }
                } else {
                    isExpand = true;
                    rl_collapse.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {

                }
            }
        });

        fab_play_expand.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(BaseActivity.this, R.color.colorPrimary)));

        imageView_play.setOnClickListener(this);
        fab_play_expand.setOnClickListener(this);
        imageView_next.setOnClickListener(this);
        imageView_next_expand.setOnClickListener(this);
        imageView_previous.setOnClickListener(this);
        imageView_previous_expand.setOnClickListener(this);
        imageView_fav.setOnClickListener(this);
        imageView_share.setOnClickListener(this);
        imageView_collapse.setOnClickListener(this);
        imageView_desc.setOnClickListener(this);
        imageView_volume.setOnClickListener(this);

        try {
            if (Constant.isFromPush) {
                Constant.isFromPush = false;
                progressDialog.show();
                loadRadio();
            }
        } catch (Exception e) {
            Constant.isFromPush = false;
            e.printStackTrace();
        }
        isLoaded = true;

        getSupportActionBar().setTitle(getString(R.string.home));


    }

    private void addView(){
        view_lollipop = findViewById(R.id.view_lollipop);

        drawer = findViewById(R.id.drawer_layout);

        tabLayout= findViewById(R.id.tabs_home);
        viewPager= findViewById(R.id.viewpage_contenido);

        textViewToolbar= findViewById(R.id.text_toolbar);

        vusikView = findViewById(R.id.vusik);

        circularProgressBar = findViewById(R.id.loader);
        circularProgressBar_collapse = findViewById(R.id.loader_collapse);
        seekbar_song = findViewById(R.id.seekbar_song);
        imageView_share = findViewById(R.id.imageView_share);
        imageView_fav = findViewById(R.id.imageView_fav_expand);
        imageView_player = findViewById(R.id.imageView_player);
        imageView_previous = findViewById(R.id.imageView_player_previous);
        imageView_previous_expand = findViewById(R.id.imageView_previous_expand);
        imageView_next = findViewById(R.id.imageView_player_next);
        imageView_next_expand = findViewById(R.id.imageView_next_expand);
        imageView_play = findViewById(R.id.imageView_player_play);
        imageView_desc = findViewById(R.id.imageView_desc_expand);
        imageView_volume = findViewById(R.id.imageView_volume);
        textView_name = findViewById(R.id.textView_player_name);
        textView_song = findViewById(R.id.textView_song_name);
        imageView_collapse = findViewById(R.id.imageView_collapse);

        fab_play_expand = findViewById(R.id.fab_play);
        imageView_radio = findViewById(R.id.imageView_radio);
        textView_radio_expand = findViewById(R.id.textView_radio_name_expand);
        textView_radio_expand2 = findViewById(R.id.textView_radio_expand);
        textView_freq_expand = findViewById(R.id.textView_freq_expand);
        textView_song_expand = findViewById(R.id.textView_song_expand);
        textView_song_duration = findViewById(R.id.textView_song_duration);
        textView_total_duration = findViewById(R.id.textView_total_duration);

        ll_player_expand = findViewById(R.id.ll_player_expand);
        ll_play_collapse = findViewById(R.id.ll_play_collapse);
        rl_song_seekbar = findViewById(R.id.rl_song_seekbar);
        rl_collapse = findViewById(R.id.ll_collapse);
        rl_expand = findViewById(R.id.ll_expand);
        rl_expand.setAlpha(0.0f);
        ll_ad = findViewById(R.id.ll_adView);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageView_player_play:
                if (Constant.playTypeRadio) {
                    clickPlayPause();
                } else {
                    clickPlayPauseOnDemand();
                }
                break;

            case R.id.fab_play:
                if (Constant.playTypeRadio) {
                    clickPlayPause();
                } else {
                    clickPlayPauseOnDemand();
                }
                break;


            case R.id.imageView_player_next:
                if (Constant.arrayList_radio.size() > 0) {
                    if (Constant.pos != Constant.arrayList_radio.size() - 1) {
                        Constant.pos = Constant.pos + 1;
                    } else {
                        Constant.pos = 0;
                    }

                    if (Constant.playTypeRadio) {
                        clickRadioStation(Constant.pos);
                    } else {
                        clickRadioStationOnDemand(Constant.pos);
                    }
                }
                break;


            case R.id.imageView_next_expand:
                if (Constant.arrayList_radio.size() > 0) {
                    if (Constant.pos != Constant.arrayList_radio.size() - 1) {
                        Constant.pos = Constant.pos + 1;
                    } else {
                        Constant.pos = 0;
                    }

                    if (Constant.playTypeRadio) {
                        clickRadioStation(Constant.pos);
                    } else {
                        clickRadioStationOnDemand(Constant.pos);
                    }
                }
                break;


            case R.id.imageView_player_previous:
                if (Constant.arrayList_radio.size() > 0) {
                    if (Constant.pos != 0) {
                        Constant.pos = Constant.pos - 1;
                    } else {
                        Constant.pos = Constant.arrayList_radio.size() - 1;
                    }

                    if (Constant.playTypeRadio) {
                        clickRadioStation(Constant.pos);
                    } else {
                        clickRadioStationOnDemand(Constant.pos);
                    }
                }
                break;


            case R.id.imageView_previous_expand:
                if (Constant.arrayList_radio.size() > 0) {
                    if (Constant.pos != 0) {
                        Constant.pos = Constant.pos - 1;
                    } else {
                        Constant.pos = Constant.arrayList_radio.size() - 1;
                    }

                    if (Constant.playTypeRadio) {
                        clickRadioStation(Constant.pos);
                    } else {
                        clickRadioStationOnDemand(Constant.pos);
                    }
                }
                break;

            case R.id.imageView_fav_expand:
                if (PlayService.getInstance().getPlayingRadioStation() != null) {
                    if (dbHelper.addORremoveFav(Constant.arrayList_radio.get(Constant.pos))) {
                        imageView_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_corazon));
                        Toast.makeText(BaseActivity.this, getString(R.string.add_to_fav), Toast.LENGTH_SHORT).show();
                    } else {
                        imageView_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_corazon_empy));
                        Toast.makeText(BaseActivity.this, getString(R.string.remove_from_fav), Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.imageView_collapse:
                slidingPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                break;

            case R.id.imageView_share:
                if (Constant.arrayList_radio.size() > 0) {
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.putExtra(Intent.EXTRA_TEXT, getString(R.string.listining_to) + " - " + Constant.arrayList_radio.get(Constant.pos).getRadioName() + "\n" + getString(R.string.app_name) + " - http://play.google.com/store/apps/details?id=" + getPackageName());
                    startActivity(share);
                }
                break;


            case R.id.imageView_desc_expand:
                showBottomSheetDialog();
                break;


            case R.id.imageView_volume:
                changeVolume();
                break;
        }
    }

    ////////////////////////////////////// MENU NAVEGATION DRAWER///////////////////////////////////
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        clickNav(item.getItemId());
        return true;
    }

    private void clickNav(int item) {
        drawer.closeDrawer(GravityCompat.START);

        switch (item) {
            case R.id.nav_settings:
                goToSettings();
                break;

            case R.id.nav_rate:
               goToRateApp();
                break;

            case R.id.nav_shareapp:
                goToShareApp();
                break;

            case R.id.nav_fb:
                gotToWeb(getString(R.string.url_facebook));
                break;
            case R.id.nav_twitter:
                gotToWeb(getString(R.string.url_twitter));
                break;
        }
    }

    private void goToSettings(){
        Intent intent = new Intent(BaseActivity.this, Activity_Setting.class);
        startActivity(intent);
    }

    private void goToRateApp(){
        final String appName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id="
                            + appName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id="
                            + appName)));
        }
    }

    private void goToShareApp(){
        Intent ishare = new Intent(Intent.ACTION_SEND);
        ishare.setType("text/plain");
        ishare.putExtra(Intent.EXTRA_TEXT, getString(R.string.app_name) + " - http://play.google.com/store/apps/details?id=" + getPackageName());
        startActivity(ishare);
    }

    private void gotToWeb(String URL){
        Intent intent_t = new Intent(Intent.ACTION_VIEW);
        intent_t.setData(Uri.parse(URL));
        startActivity(intent_t);

    }

    /////////////////////////// OPCIONES WIDGET RADIO///////////////////////////////////////////////
    private void clickPlayPause() {
        ItemRadio itemRadio = PlayService.getInstance().getPlayingRadioStation();
        if (itemRadio != null) {
            Intent intent = new Intent(BaseActivity.this, PlayService.class);
            if (Constant.isPlaying) {
                vusikView.pauseNotesFall();
                intent.setAction(PlayService.ACTION_STOP);
                startService(intent);
            } else {
                vusikView.start();
                PlayService.initialize(BaseActivity.this, itemRadio);
                intent.setAction(PlayService.ACTION_PLAY);
                startService(intent);
            }
        } else {
            if (Constant.arrayList_radio.size() > 0) {
                Intent intent = new Intent(BaseActivity.this, PlayService.class);
                PlayService.initialize(BaseActivity.this, Constant.arrayList_radio.get(Constant.pos));
                intent.setAction(PlayService.ACTION_PLAY);
                startService(intent);
            } else {
                jsonUtils.showToast(getString(R.string.no_radio_selected));
            }
        }
    }

    private void clickPlayPauseOnDemand() {
        ItemRadio itemRadio = OnDemandPlayService.getInstance().getPlayingRadioStation();
        if (itemRadio != null && Constant.isPlayed) {
            Intent intent = new Intent(BaseActivity.this, OnDemandPlayService.class);
            if (Constant.isPlaying) {
                vusikView.pauseNotesFall();
                intent.setAction(OnDemandPlayService.ACTION_PAUSE);
                startService(intent);
            } else {
                vusikView.start();
                OnDemandPlayService.initialize(BaseActivity.this, itemRadio);
                intent.setAction(OnDemandPlayService.ACTION_PLAY);
                startService(intent);
            }
        } else {
            if (Constant.arrayList_radio.size() > 0) {
                Intent intent = new Intent(BaseActivity.this, OnDemandPlayService.class);
                OnDemandPlayService.initialize(BaseActivity.this, Constant.arrayList_radio.get(Constant.pos));
                if (Constant.isPlayed) {
                    intent.setAction(OnDemandPlayService.ACTION_PLAY);
                } else {
                    intent.setAction(OnDemandPlayService.ACTION_FIRST_PLAY);
                }
                startService(intent);
            } else {
                jsonUtils.showToast(getString(R.string.no_radio_selected));
            }
        }
    }

    public void clickRadioStation(int position) {
        if (!Constant.playTypeRadio) {
            if (Constant.isPlaying) {
                Constant.isPlaying = false;
                Intent intent = new Intent(BaseActivity.this, OnDemandPlayService.class);
                intent.setAction(PlayService.ACTION_STOP);
                startService(intent);
            }
        }
        Constant.playTypeRadio = true;
        Constant.pos = position;
        ItemRadio radio = Constant.arrayList_radio.get(Constant.pos);
        final Intent intent = new Intent(BaseActivity.this, PlayService.class);
        if (Constant.isPlaying) {
            if (!radio.getRadioName().equals(PlayService.getInstance().getPlayingRadioStation().getRadioName())) {
                PlayService.initialize(BaseActivity.this, radio);
                intent.setAction(PlayService.ACTION_NEW);
                startService(intent);
            } else {
                intent.setAction(PlayService.ACTION_STOP);
                startService(intent);
            }
        } else {
            PlayService.initialize(BaseActivity.this, radio);
            intent.setAction(PlayService.ACTION_PLAY);
            startService(intent);
        }
    }

    public void clickRadioStationOnDemand(int position) {
        if (Constant.playTypeRadio) {
            if (Constant.isPlaying) {
                Constant.isPlaying = false;
                Intent intent = new Intent(BaseActivity.this, PlayService.class);
                intent.setAction(PlayService.ACTION_STOP);
                startService(intent);
            }
        }
        Constant.playTypeRadio = false;
        Constant.pos = position;
        ItemRadio radio = Constant.arrayList_radio.get(Constant.pos);

        final Intent intent = new Intent(BaseActivity.this, OnDemandPlayService.class);
        if (Constant.isPlaying) {
            if (!radio.getRadioName().equals(OnDemandPlayService.getInstance().getPlayingRadioStation().getRadioName())) {
                OnDemandPlayService.initialize(BaseActivity.this, radio);
                intent.setAction(OnDemandPlayService.ACTION_FIRST_PLAY);
                startService(intent);
            } else {
                intent.setAction(OnDemandPlayService.ACTION_PAUSE);
                startService(intent);
            }
        } else {
            OnDemandPlayService.initialize(BaseActivity.this, radio);
            intent.setAction(OnDemandPlayService.ACTION_FIRST_PLAY);
            startService(intent);
        }
    }

    public void changePlayPause(Boolean flag) {
        Constant.isPlaying = flag;

        if (flag) {
            ItemRadio itemRadio;
            if (Constant.playTypeRadio) {
                itemRadio = PlayService.getInstance().getPlayingRadioStation();
            } else {
                itemRadio = OnDemandPlayService.getInstance().getPlayingRadioStation();
            }
            if (itemRadio != null) {
                changeText(itemRadio);
                imageView_play.setImageDrawable(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_pausa));
                fab_play_expand.setImageDrawable(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_pausa));
            }
        } else {
            if (Constant.arrayList_radio.size() > 0) {
                changeText(Constant.arrayList_radio.get(Constant.pos));
            }
            imageView_play.setImageDrawable(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_play_empy));
            fab_play_expand.setImageDrawable(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_play));
        }
    }

    public void changeText(ItemRadio itemRadio) {
        if (Constant.playTypeRadio) {
            textView_freq_expand.setText(itemRadio.getRadioFreq() + " " + getString(R.string.HZ));
            textView_radio_expand2.setText(getString(R.string.radio));
            changeSongName(Constant.song_name);
            changeFav(itemRadio.getRadioId());

            textView_freq_expand.setVisibility(View.VISIBLE);
            textView_song_expand.setVisibility(View.VISIBLE);
            imageView_fav.setVisibility(View.VISIBLE);
            rl_song_seekbar.setVisibility(View.GONE);

            if (FragmentRadio.adapterRadioList != null && FragmentRadio.adapterRadioList_mostview != null) {
                FragmentRadio.adapterRadioList.notifyDataSetChanged();
                FragmentRadio.adapterRadioList_mostview.notifyDataSetChanged();
                FragmentRadio.adapterRadioList_featured.notifyDataSetChanged();
            }
        } else {
            textView_total_duration.setText(itemRadio.getDuration());
            textView_radio_expand2.setText(getString(R.string.on_demand));
            textView_song.setText(getString(R.string.on_demand));
            textView_song_expand.setText(itemRadio.getRadioName());

            textView_freq_expand.setVisibility(View.GONE);
            textView_song_expand.setVisibility(View.GONE);
            imageView_fav.setVisibility(View.INVISIBLE);
            rl_song_seekbar.setVisibility(View.VISIBLE);
        }
        textView_name.setText(itemRadio.getRadioName());
        textView_radio_expand.setText(itemRadio.getRadioName());

        Picasso.get().load(itemRadio.getRadioImageurl())
                .placeholder(R.drawable.ic_carga)
                .into(imageView_radio);

        Picasso.get().load(itemRadio.getRadioImageurl())
                .placeholder(R.drawable.ic_carga)
                .into(imageView_player);

    }

    public void changeFav(String id) {
        if (dbHelper.checkFav(id)) {
            imageView_fav.setImageDrawable(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_corazon));
        } else {
            imageView_fav.setImageDrawable(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_corazon_empy));
        }
    }

    public void changeSongName(String songName) {
        Constant.song_name = songName;
        textView_song.setText(songName);
        textView_song_expand.setText(songName);
    }

    public void setIfPlaying() {
        if (Constant.isPlaying) {
            if (Constant.playTypeRadio) {
                PlayService.initNewContext(BaseActivity.this);
            } else {
                OnDemandPlayService.initNewContext(BaseActivity.this);
                seekUpdation();
            }
        }
        changePlayPause(Constant.isPlaying);
    }

    public void loadAboutData() {
        loadAbout = new LoadAbout(new AboutListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onEnd(String success) {
                adConsent.checkForConsent();
                dbHelper.addtoAbout();
            }
        });
        loadAbout.execute(Constant.APP_DETAILS_URL);
    }

    ///// DIALOGO SALIR DE LA APP
    public void openQuitDialog() {
        AlertDialog.Builder alert;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alert = new AlertDialog.Builder(BaseActivity.this, R.style.QuitDialog);
        } else {
            alert = new AlertDialog.Builder(BaseActivity.this);
        }
        alert.setTitle(R.string.app_name);
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setMessage(getString(R.string.sure_quit));

        alert.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                finish();
            }
        });

        alert.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.show();
    }

    private InterAdListener interAdListener = new InterAdListener() {
        @Override
        public void onClick(int position, String type) {
            if (type.equals("nav")) {
                clickNav(position);
            }
        }
    };

    ////PERMISOS DE LA APP
    public void checkPer() {
        if ((ContextCompat.checkSelfPermission(BaseActivity.this, "android.permission.WRITE_EXTERNAL_STORAGE") != PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(BaseActivity.this, "android.permission.READ_PHONE_STATE") != PackageManager.PERMISSION_GRANTED)) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_PHONE_STATE"}, 1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        try {
            Constant.isFromPush = intent.getExtras().getBoolean("ispushnoti", false);
            if (Constant.isFromPush) {
                Constant.isFromPush = false;
                progressDialog.show();
                loadRadio();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onNewIntent(intent);
    }

    private void loadRadio() {
        LoadRadioViewed loadRadioViewed = new LoadRadioViewed(new RadioViewListener() {
            @Override
            public void onEnd(String success) {
                progressDialog.dismiss();
                if (success.equals("1")) {
                    Constant.arrayList_radio.clear();
                    Constant.arrayList_radio.add(Constant.itemRadio);
                    clickRadioStation(0);
                }
            }
        });
        loadRadioViewed.execute(Constant.RADIO_URL + Constant.pushRID);
    }

    public void showBottomSheetDialog() {
        View view = getLayoutInflater().inflate(R.layout.layout_desc, null);

        dialog_desc = new BottomSheetDialog(this);
        dialog_desc.setContentView(view);
        dialog_desc.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        dialog_desc.show();

        AppCompatButton button = dialog_desc.findViewById(R.id.button_detail_close);
        ViewCompat.setBackgroundTintList(button, ColorStateList.valueOf(sharedPref.getFirstColor()));
        TextView textView = dialog_desc.findViewById(R.id.textView_detail_title);
        textView.setText(Constant.arrayList_radio.get(Constant.pos).getRadioName());

        textView.setBackground(jsonUtils.getGradientDrawableToolbar());

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_desc.dismiss();
            }
        });

        WebView webview_song_desc = dialog_desc.findViewById(R.id.webView_bottom);
        String mimeType = "text/html;charset=UTF-8";
        String encoding = "utf-8";
        String text = "<html><head>"
                + "<style> body{color: #000 !important;text-align:left}"
                + "</style></head>"
                + "<body>"
                + Constant.arrayList_radio.get(Constant.pos).getDescription()
                + "</body></html>";

        webview_song_desc.loadData(text, mimeType, encoding);
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (slidingPanel.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            slidingPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else if (fm.getBackStackEntryCount() != 0) {
            getSupportActionBar().setTitle(fm.getFragments().get(fm.getBackStackEntryCount() - 1).getTag());
            super.onBackPressed();
        } else {
            openQuitDialog();
        }
    }

    private Runnable run = new Runnable() {
        @Override
        public void run() {
            try {
                seekUpdation();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void seekUpdation() {
        seekbar_song.setProgress(JsonUtils.getProgressPercentage(Constant.exoPlayer_ondemand.getCurrentPosition(), JsonUtils.calculateTime(Constant.arrayList_radio.get(Constant.pos).getDuration())));
        textView_song_duration.setText(JsonUtils.milliSecondsToTimer(Constant.exoPlayer_ondemand.getCurrentPosition()));
        Log.e("duration", "" + JsonUtils.milliSecondsToTimer(Constant.exoPlayer_ondemand.getCurrentPosition()));
        seekbar_song.setSecondaryProgress(Constant.exoPlayer_ondemand.getBufferedPercentage());
        if (Constant.isPlaying) {
            seekHandler.postDelayed(run, 1000);
        }
    }

    public void setBuffer(Boolean flag) {
        if (flag) {
            circularProgressBar.setVisibility(View.VISIBLE);
            ll_player_expand.setVisibility(View.INVISIBLE);
            circularProgressBar_collapse.setVisibility(View.VISIBLE);
            ll_play_collapse.setVisibility(View.INVISIBLE);
        } else {
            circularProgressBar.setVisibility(View.INVISIBLE);
            ll_player_expand.setVisibility(View.VISIBLE);
            circularProgressBar_collapse.setVisibility(View.INVISIBLE);
            ll_play_collapse.setVisibility(View.VISIBLE);
        }
    }

    public void changeVolume() {
        final RelativePopupWindow popupWindow = new RelativePopupWindow(this);

        // inflate your layout or dynamically add view
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.view_volumen, null);
        ImageView imageView1 = view.findViewById(R.id.iv1);
        ImageView imageView2 = view.findViewById(R.id.iv2);
        imageView1.setColorFilter(Color.BLACK);
        imageView2.setColorFilter(Color.BLACK);

        VerticalSeekBar seekBar = view.findViewById(R.id.seekbar_volume);
        seekBar.getThumb().setColorFilter(sharedPref.getFirstColor(), PorterDuff.Mode.SRC_IN);
        seekBar.getProgressDrawable().setColorFilter(sharedPref.getSecondColor(), PorterDuff.Mode.SRC_IN);

        final AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        seekBar.setMax(am.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        int volume_level = am.getStreamVolume(AudioManager.STREAM_MUSIC);
        seekBar.setProgress(volume_level);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                am.setStreamVolume(AudioManager.STREAM_MUSIC, i, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        popupWindow.setFocusable(true);
        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popupWindow.setContentView(view);
        popupWindow.showOnAnchor(imageView_volume, RelativePopupWindow.VerticalPosition.ABOVE, RelativePopupWindow.HorizontalPosition.CENTER);
    }

    @Override
    protected void onResume() {
        Constant.isQuitDialog = true;
        setIfPlaying();

        if (isLoaded && Constant.isThemeChanged) {
            if (FragmentCity.adapterCity != null) {
                FragmentCity.adapterCity.notifyDataSetChanged();
                ViewCompat.setBackgroundTintList(FragmentCity.button_try, ColorStateList.valueOf(sharedPref.getFirstColor()));
            }
            if (FragmentLanguage.adapterLanguage != null) {
                FragmentLanguage.adapterLanguage.notifyDataSetChanged();
                ViewCompat.setBackgroundTintList(FragmentLanguage.button_try, ColorStateList.valueOf(sharedPref.getFirstColor()));
            }
            /*if(FragmentMain.appBarLayout != null) {
                FragmentMain.appBarLayout.setBackground(jsonUtils.getGradientDrawableToolbar());
            }*/

            if(FragmentOnDemandCat.button_try != null) {
                ViewCompat.setBackgroundTintList(FragmentOnDemandCat.button_try, ColorStateList.valueOf(sharedPref.getFirstColor()));
            }

            if(FragmentOnDemandDetails.button_try != null) {
                ViewCompat.setBackgroundTintList(FragmentOnDemandDetails.button_try, ColorStateList.valueOf(sharedPref.getFirstColor()));
            }

            if(FragmentSearch.button_try != null) {
                ViewCompat.setBackgroundTintList(FragmentSearch.button_try, ColorStateList.valueOf(sharedPref.getFirstColor()));
            }

            if(FragmentFeaturedRadio.button_try != null) {
                ViewCompat.setBackgroundTintList(FragmentFeaturedRadio.button_try, ColorStateList.valueOf(sharedPref.getFirstColor()));
            }

            if(FragmentLanguageDetails.button_try != null) {
                ViewCompat.setBackgroundTintList(FragmentLanguageDetails.button_try, ColorStateList.valueOf(sharedPref.getFirstColor()));
            }
        }
        ////////////////////////////////////////////////////////////////////////navigationView.setCheckedItem(navigationView.getMenu().findItem(R.id.nav_home).getItemId());
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.clock) {

            FragmentManager fm = getSupportFragmentManager();
            NumberPickerDialog newFragment = new NumberPickerDialog();
            newFragment.setValueChangeListener(this);
            newFragment.show( getFragmentManager() ,"Timer");

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i1) {
        //Toast.makeText(this, "selected number " + numberPicker.getValue(), Toast.LENGTH_SHORT).show();
    }

    ///////////////////////// TABS /////////////////////////////////////////////////////////////////

    private void gestorTab(){

        viewPager.setOffscreenPageLimit(3);
        loadViewPag(viewPager);

        tabLayout.setupWithViewPager(viewPager);

        addTabIcons();


        textViewToolbar.setText(R.string.radio);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition()==0){
                    textViewToolbar.setText(R.string.countrys);
                }else if(tab.getPosition()==1){
                    textViewToolbar.setText(R.string.radio);
                }else if (tab.getPosition()==2){
                    textViewToolbar.setText(R.string.favourite);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        tabLayout.getTabAt(1).select();
    }


    private void loadViewPag(ViewPager viewPager){
        FragmentCity cityFragment=  new FragmentCity();
        FragmentRadio radioFragment= new FragmentRadio();
        FragmentFavorite favFragment= new FragmentFavorite();

        ViewPageAdapter adapter= new ViewPageAdapter(getSupportFragmentManager());
        adapter.addFragmnet(cityFragment);
        adapter.addFragmnet(radioFragment);
        adapter.addFragmnet(favFragment);

        viewPager.setAdapter(adapter);
    }


    private void addTabIcons(){
        for(int i=0;i<3;i++){
            tabLayout.getTabAt(i).setIcon(tabsIconos[i]);
        }
    }

}