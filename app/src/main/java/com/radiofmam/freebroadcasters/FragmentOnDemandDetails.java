package com.radiofmam.freebroadcasters;

import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.radiofmam.adapter.AdapterOnDemand;
import com.radiofmam.item.ItemOnDemandCat;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.DBHelper;
import com.radiofmam.utils.JsonUtils;
import com.radiofmam.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

public class FragmentOnDemandDetails extends Fragment {

    DBHelper dbHelper;
    RecyclerView recyclerView;
    AdapterOnDemand adapter;
    GridLayoutManager lLayout;
    ImageView imageView_ondemand;
    SearchView searchView;
    ArrayList<ItemRadio> arraylist;
    ItemOnDemandCat itemOnDemandCat;
    CircularProgressBar progressBar;
    TextView textView_empty;
    public static AppCompatButton button_try;
    LinearLayout ll_empty;
    String errr_msg;
    SharedPref sharedPref;
    JsonUtils jsonUtils;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_ondemand, container, false);

        itemOnDemandCat = (ItemOnDemandCat) getArguments().getSerializable("item");

        dbHelper = new DBHelper(getActivity());
        jsonUtils = new JsonUtils(getActivity());
        sharedPref = new SharedPref(getActivity());

        arraylist = new ArrayList<>();
        imageView_ondemand = rootView.findViewById(R.id.imageView_ondemand);
        progressBar = rootView.findViewById(R.id.progressBar_on);

        ll_empty = rootView.findViewById(R.id.ll_empty);
        textView_empty = rootView.findViewById(R.id.textView_empty_msg);
        button_try = rootView.findViewById(R.id.button_empty_try);
        ViewCompat.setBackgroundTintList(button_try, ColorStateList.valueOf(sharedPref.getFirstColor()));

        Picasso.get()
                .load(itemOnDemandCat.getImage())
                .into(imageView_ondemand);

        lLayout = new GridLayoutManager(getActivity(), 1);

        recyclerView = rootView.findViewById(R.id.recyclerView_on);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lLayout);

        loadOnDemand();

        button_try.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadOnDemand();
            }
        });

        setHasOptionsMenu(true);
        return rootView;
    }

    private void loadOnDemand() {
        if (JsonUtils.isNetworkAvailable(getActivity())) {
            new MyTask().execute(Constant.URL_ON_DEMAND + itemOnDemandCat.getId());
        } else {
            errr_msg = getString(R.string.internet_not_connected);
            setEmpty();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.search);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);

        searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(queryTextListener);
        super.onCreateOptionsMenu(menu, inflater);
    }

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            if (arraylist.size() > 0) {
                if (searchView.isIconified()) {
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    adapter.getFilter().filter(s);
                    adapter.notifyDataSetChanged();
                }
            }
            return true;
        }
    };

    private class MyTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            arraylist.clear();
            ll_empty.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            return JsonUtils.getJSONString(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            if(getActivity()!= null) {
                super.onPostExecute(result);
                if (null == result || result.length() == 0) {
                    jsonUtils.showToast(getString(R.string.items_not_found));
                } else {
                    try {
                        JSONObject mainJson = new JSONObject(result);
                        JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);
                        JSONObject objJson;

                        for (int i = 0; i < jsonArray.length(); i++) {
                            objJson = jsonArray.getJSONObject(i);

                            String id = objJson.getString("id");
                            String name = objJson.getString("mp3_title");
                            String url = objJson.getString("mp3_url");
                            String image = objJson.getString("mp3_thumbnail_b");
                            String thumb = objJson.getString("mp3_thumbnail_s");
                            String duration = objJson.getString("mp3_duration");
                            String desc = objJson.getString("mp3_description");
                            String totalviews = objJson.getString("total_views");
                            ItemRadio objItem = new ItemRadio(id, name, url, image, thumb, duration, totalviews, desc, "ondemand");
                            arraylist.add(objItem);
                        }
                        errr_msg = getString(R.string.items_not_found);
                    } catch (Exception e) {
                        errr_msg = getString(R.string.server_error);
                        e.printStackTrace();
                    }
                    setAdapterToListview();
                }
            }
        }
    }

    public void setAdapterToListview() {
        adapter = new AdapterOnDemand(getActivity(), arraylist);
        recyclerView.setAdapter(adapter);
        setEmpty();
    }

    public void setEmpty() {
        progressBar.setVisibility(View.GONE);
        if (arraylist.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ll_empty.setVisibility(View.GONE);
        } else {
            textView_empty.setText(errr_msg);
            recyclerView.setVisibility(View.GONE);
            ll_empty.setVisibility(View.VISIBLE);
        }
    }
}