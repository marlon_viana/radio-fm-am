package com.radiofmam.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.radiofmam.interfaces.CityToCityDetail;
import com.squareup.picasso.Picasso;
import com.radiofmam.interfaces.CityClickListener;
import com.radiofmam.interfaces.InterAdListener;
import com.radiofmam.item.ItemCity;
import com.radiofmam.freebroadcasters.R;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.JsonUtils;
import com.radiofmam.utils.SharedPref;

import java.util.ArrayList;


public class AdapterCity extends RecyclerView.Adapter<AdapterCity.MyViewHolder> {

    private ArrayList<ItemCity> arraylist;
    private ArrayList<ItemCity> filteredArrayList;
    private NameFilter filter;
    private JsonUtils jsonUtils;
    private SharedPref sharedPref;
    private CityToCityDetail cityToCityDetail;
    private Context context;

    public AdapterCity(Context context, ArrayList<ItemCity> list, CityToCityDetail cityToCityDetail) {
        this.arraylist = list;
        this.arraylist = list;
        this.filteredArrayList = list;
        jsonUtils = new JsonUtils(context, interAdListener);
        sharedPref = new SharedPref(context);
        this.context=context;
        this.cityToCityDetail= cityToCityDetail;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView textView_title;
        private ConstraintLayout ll;
        private ImageView vieww;

        private MyViewHolder(View view) {
            super(view);
            ll = view.findViewById(R.id.ll);
            textView_title = view.findViewById(R.id.textView_cityname);
            vieww = view.findViewById(R.id.view_city);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_city, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int i) {


        Glide.with(context)
                .load(Constant.SERVER_URL+"/assets/"+arraylist.get(i).getTagLine())
                .into(holder.vieww);

        holder.textView_title.setText(arraylist.get(i).getName());

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("COUNTR NAME",arraylist.get(i).getName());
                cityToCityDetail.selctCity(arraylist.get(i).getName());
                jsonUtils.showInterAd(holder.getAdapterPosition(), "",0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    private String getID(int pos) {
        return arraylist.get(pos).getId();
    }

    public Filter getFilter() {
        if (filter == null) {
            filter = new NameFilter();
        }
        return filter;
    }

    private class NameFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (constraint.toString().length() > 0) {
                ArrayList<ItemCity> filteredItems = new ArrayList<>();

                for (int i = 0, l = filteredArrayList.size(); i < l; i++) {
                    String nameList = filteredArrayList.get(i).getName();
                    if (nameList.toLowerCase().contains(constraint))
                        filteredItems.add(filteredArrayList.get(i));
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            } else {
                synchronized (this) {
                    result.values = filteredArrayList;
                    result.count = filteredArrayList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            arraylist = (ArrayList<ItemCity>) results.values;
            notifyDataSetChanged();
        }
    }

    private void loadCityRadio(int pos) {
        int reali = getPosition(getID(pos));
        Constant.itemCity = arraylist.get(reali);

    }

    private int getPosition(String id) {
        int count = 0;
        int rid = Integer.parseInt(id);
        for (int i = 0; i < arraylist.size(); i++) {
            try {
                if (rid == Integer.parseInt(arraylist.get(i).getId())) {
                    count = i;
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    private InterAdListener interAdListener = new InterAdListener() {
        @Override
        public void onClick(int position, String type) {
            loadCityRadio(position);
        }
    };
}