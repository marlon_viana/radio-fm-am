package com.radiofmam.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.radiofmam.interfaces.CityClickListener;
import com.radiofmam.interfaces.InterAdListener;
import com.radiofmam.item.ItemLanguage;
import com.radiofmam.freebroadcasters.R;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.JsonUtils;
import com.radiofmam.utils.SharedPref;

import java.util.ArrayList;


public class AdapterLanguage extends RecyclerView.Adapter<AdapterLanguage.MyViewHolder> {

    private ArrayList<ItemLanguage> arraylist;
    private ArrayList<ItemLanguage> filteredArrayList;
    private NameFilter filter;
    private CityClickListener cityClickListener;
    private JsonUtils jsonUtils;
    private SharedPref sharedPref;

    public AdapterLanguage(Context context, ArrayList<ItemLanguage> list, CityClickListener cityClickListener) {
        this.arraylist = list;
        this.arraylist = list;
        this.filteredArrayList = list;
        this.cityClickListener = cityClickListener;
        jsonUtils = new JsonUtils(context, interAdListener);
        sharedPref = new SharedPref(context);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView textView_title;
        private LinearLayout ll;
        private View vieww;

        private MyViewHolder(View view) {
            super(view);
            ll = view.findViewById(R.id.ll);
            textView_title = view.findViewById(R.id.textView_cityname);
            vieww = view.findViewById(R.id.view_city);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_city, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        holder.vieww.setBackgroundColor(sharedPref.getFirstColor());
        holder.textView_title.setText(arraylist.get(position).getName());

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jsonUtils.showInterAd(holder.getAdapterPosition(), "",0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    private String getID(int pos) {
        return arraylist.get(pos).getId();
    }

    public Filter getFilter() {
        if (filter == null) {
            filter = new NameFilter();
        }
        return filter;
    }

    private class NameFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (constraint.toString().length() > 0) {
                ArrayList<ItemLanguage> filteredItems = new ArrayList<>();

                for (int i = 0, l = filteredArrayList.size(); i < l; i++) {
                    String nameList = filteredArrayList.get(i).getName();
                    if (nameList.toLowerCase().contains(constraint))
                        filteredItems.add(filteredArrayList.get(i));
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            } else {
                synchronized (this) {
                    result.values = filteredArrayList;
                    result.count = filteredArrayList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            arraylist = (ArrayList<ItemLanguage>) results.values;
            notifyDataSetChanged();
        }
    }

    private void loadLangRadio(int pos) {
        int reali = getPosition(getID(pos));
        Constant.itemLanguage = arraylist.get(reali);
        cityClickListener.onClick();
    }

    private int getPosition(String id) {
        int count = 0;
        int rid = Integer.parseInt(id);
        for (int i = 0; i < arraylist.size(); i++) {
            try {
                if (rid == Integer.parseInt(arraylist.get(i).getId())) {
                    count = i;
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    private InterAdListener interAdListener = new InterAdListener() {
        @Override
        public void onClick(int position, String type) {
            loadLangRadio(position);
        }
    };
}