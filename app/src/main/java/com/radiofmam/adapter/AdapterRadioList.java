package com.radiofmam.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.radiofmam.interfaces.InterAdListener;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.freebroadcasters.BaseActivity;
import com.radiofmam.freebroadcasters.PlayService;
import com.radiofmam.freebroadcasters.R;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.DBHelper;
import com.radiofmam.utils.JsonUtils;

import java.util.ArrayList;


public class AdapterRadioList extends RecyclerView.Adapter<AdapterRadioList.MyViewHolder> {

    private DBHelper dbHelper;
    private ArrayList<ItemRadio> arrayList;
    private Context context;
    private ArrayList<ItemRadio> filteredArrayList;
    private NameFilter filter;
    private JsonUtils jsonUtils;
    private int intSelect=-1;

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView, imageView_fav;
        private TextView textView_radio, textView_freq;
        private ConstraintLayout rl_home;

        private MyViewHolder(View view) {
            super(view);
            rl_home = view.findViewById(R.id.rl_home);
            textView_radio = view.findViewById(R.id.textView_radio_home);
            textView_freq = view.findViewById(R.id.textView_freq_home);
            imageView = view.findViewById(R.id.imageView_radio_home);
            imageView_fav = view.findViewById(R.id.imageView_fav_home);
        }
    }

    public AdapterRadioList(Context context, ArrayList<ItemRadio> arrayList) {
        this.arrayList = arrayList;
        this.context = context;
        jsonUtils = new JsonUtils(context, interAdListener);
        filteredArrayList = arrayList;
        dbHelper = new DBHelper(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_radio, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        ItemRadio objAllBean = arrayList.get(position);
        Boolean isFav = checkFav(position);

        /*try {
            if (Constant.playTypeRadio && Constant.isPlaying && PlayService.getInstance().getPlayingRadioStation().getRadioId().equals(objAllBean.getRadioId())) {
              //  holder.rl_home.setBackgroundColor(ContextCompat.getColor(context, R.color.bg_playing));
            } else {
              //  holder.rl_home.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        if (position==intSelect){
            holder.rl_home.setBackground(context.getDrawable(R.drawable.dw_gradient_primary));
        }


        if (isFav) {
            holder.imageView_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_corazon));
        } else {
            holder.imageView_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_corazon_empy));
        }

        holder.textView_radio.setText(objAllBean.getRadioName());
        holder.textView_freq.setText(objAllBean.getRadioFreq());

        Glide.with(context)
                .load(objAllBean.getRadioImageurl().replace(" ", "%20"))
                .into(holder.imageView);

        holder.imageView_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dbHelper.addORremoveFav(arrayList.get(holder.getAdapterPosition()))) {
                    holder.imageView_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_corazon));
                    Toast.makeText(context, context.getString(R.string.add_to_fav), Toast.LENGTH_SHORT).show();
                } else {
                    holder.imageView_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_corazon_empy));
                    Toast.makeText(context, context.getString(R.string.remove_from_fav), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.rl_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intSelect= position;
                jsonUtils.showInterAd(holder.getAdapterPosition(), "",1);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    private Boolean checkFav(int pos) {
        return dbHelper.checkFav(arrayList.get(pos).getRadioId());
    }

    public Filter getFilter() {
        if (filter == null) {
            filter = new NameFilter();
        }
        return filter;
    }

    private class NameFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (constraint.toString().length() > 0) {
                ArrayList<ItemRadio> filteredItems = new ArrayList<>();

                for (int i = 0, l = filteredArrayList.size(); i < l; i++) {
                    String nameList = filteredArrayList.get(i).getRadioName();
                    if (nameList.toLowerCase().contains(constraint))
                        filteredItems.add(filteredArrayList.get(i));
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            } else {
                synchronized (this) {
                    result.values = filteredArrayList;
                    result.count = filteredArrayList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            arrayList = (ArrayList<ItemRadio>) results.values;
            notifyDataSetChanged();
        }
    }

    private InterAdListener interAdListener = new InterAdListener() {
        @Override
        public void onClick(int position, String type) {
            Constant.arrayList_radio.clear();
            Constant.arrayList_radio.addAll(arrayList);
            ((BaseActivity) context).clickRadioStation(position);
        }
    };
}