package com.radiofmam.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.radiofmam.interfaces.InterAdListener;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.freebroadcasters.BaseActivity;
import com.radiofmam.freebroadcasters.R;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.DBHelper;
import com.radiofmam.utils.JsonUtils;
import com.radiofmam.utils.SharedPref;

import java.util.ArrayList;


public class AdapterCityDetails extends RecyclerView.Adapter<AdapterCityDetails.MyViewHolder> {

    private DBHelper dbHelper;
    private Context context;
    private ArrayList<ItemRadio> arraylist;
    private ArrayList<ItemRadio> filteredArrayList;
    private NameFilter filter;
    private JsonUtils jsonUtils;
    private int intSelect=-1;

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView textView_title, textView_views, textView_lang;
        private ImageView imageView_fav, imageView;
        private CardView cardView;
        private ConstraintLayout constraintLayout;

        private MyViewHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.row_layout);
            textView_views = view.findViewById(R.id.textView_view);
            textView_lang = view.findViewById(R.id.textView_list_lang);
            textView_title = view.findViewById(R.id.textView_radio_name);
            imageView = view.findViewById(R.id.row_logo);
            imageView_fav = view.findViewById(R.id.imageView_fav);
            constraintLayout= view.findViewById(R.id.rl_home);
        }
    }

    public AdapterCityDetails(Context context, ArrayList<ItemRadio> list) {
        this.context = context;
        this.arraylist = list;
        filteredArrayList = list;
        dbHelper = new DBHelper(context);

        jsonUtils = new JsonUtils(context, interAdListener);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_cityradio_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        Boolean isFav = checkFav(position);

        if (position==intSelect){
            holder.constraintLayout.setBackground(context.getDrawable(R.drawable.dw_gradient_primary));
        }else {
            holder.constraintLayout.setBackgroundColor(context.getColor(R.color.backgroundLight));
        }


        if (isFav) {
            holder.imageView_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_corazon));
        } else {
            holder.imageView_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_corazon_empy));
        }

        holder.textView_views.setText(JsonUtils.format(Double.parseDouble(arraylist.get(position).getViews())));
        holder.textView_title.setText(arraylist.get(position).getRadioName());
        holder.textView_lang.setText(arraylist.get(position).getLanguage());

        Glide.with(context)
                .load(arraylist.get(holder.getAdapterPosition()).getRadioImageurl())
                .into(holder.imageView);


        holder.imageView_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dbHelper.addORremoveFav(arraylist.get(holder.getAdapterPosition()))) {
                    holder.imageView_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_corazon));
                    Toast.makeText(context, context.getString(R.string.add_to_fav), Toast.LENGTH_SHORT).show();

                } else {
                    holder.imageView_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_corazon_empy));
                    Toast.makeText(context, context.getString(R.string.remove_from_fav), Toast.LENGTH_SHORT).show();
                }
            }
        });


        holder.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("ENTRO","SI");
                intSelect= position;
                jsonUtils.showInterAd(holder.getAdapterPosition(), "",1);
                notifyDataSetChanged();
            }
        });

    }

    private Boolean checkFav(int pos) {
        return dbHelper.checkFav(arraylist.get(pos).getRadioId());
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public Filter getFilter() {
        if (filter == null) {
            filter = new NameFilter();
        }
        return filter;
    }

    private class NameFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (constraint.toString().length() > 0) {
                ArrayList<ItemRadio> filteredItems = new ArrayList<>();

                for (int i = 0, l = filteredArrayList.size(); i < l; i++) {
                    String nameList = filteredArrayList.get(i).getRadioName();
                    if (nameList.toLowerCase().contains(constraint))
                        filteredItems.add(filteredArrayList.get(i));
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            } else {
                synchronized (this) {
                    result.values = filteredArrayList;
                    result.count = filteredArrayList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            arraylist = (ArrayList<ItemRadio>) results.values;
            notifyDataSetChanged();
        }
    }

    private InterAdListener interAdListener = new InterAdListener() {
        @Override
        public void onClick(int position, String type) {
            Constant.arrayList_radio.clear();
            Constant.arrayList_radio.addAll(arraylist);
            ((BaseActivity) context).clickRadioStation(position);
        }
    };
}