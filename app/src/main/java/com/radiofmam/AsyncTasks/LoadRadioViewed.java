package com.radiofmam.AsyncTasks;

import android.os.AsyncTask;

import com.radiofmam.interfaces.RadioViewListener;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.JsonUtils;

import org.json.JSONArray;
import org.json.JSONObject;

public class LoadRadioViewed extends AsyncTask<String, String, String> {

    private RadioViewListener radioViewListener;

    public LoadRadioViewed(RadioViewListener radioViewListener) {
        this.radioViewListener = radioViewListener;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            String json_string = JsonUtils.getJSONString(strings[0]);

            JSONObject mainJson = new JSONObject(json_string);
            JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);
            JSONObject objJson;
            for (int i = 0; i < jsonArray.length(); i++) {
                objJson = jsonArray.getJSONObject(i);

                String id = objJson.getString(Constant.RADIO_ID);
                String name = objJson.getString(Constant.RADIO_NAME);
                String url = objJson.getString(Constant.RADIO_IMAGE);
                String freq = objJson.getString(Constant.RADIO_FREQ);
                String img = objJson.getString(Constant.RADIO_IMAGE_BIG);
                String views = objJson.getString(Constant.RADIO_VIEWS);
                String cid = objJson.getString(Constant.CITY_CID);
                String cityname = objJson.getString(Constant.CITY_NAME);
                String lang = objJson.getString(Constant.RADIO_LANG);
                String description = objJson.getString(Constant.RADIO_DESC);
                Constant.itemRadio = new ItemRadio(id,name,url,freq,img,views,cid,cityname,lang, description,"radio");
            }
            return "1";
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }

    }

    @Override
    protected void onPostExecute(String s) {
        radioViewListener.onEnd(s);
        super.onPostExecute(s);
    }
}
