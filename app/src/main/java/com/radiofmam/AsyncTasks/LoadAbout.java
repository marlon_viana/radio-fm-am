package com.radiofmam.AsyncTasks;

import android.os.AsyncTask;

import com.radiofmam.interfaces.AboutListener;
import com.radiofmam.item.ItemAbout;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.JsonUtils;

import org.json.JSONArray;
import org.json.JSONObject;

public class LoadAbout extends AsyncTask<String, String, String> {

    private AboutListener aboutListener;

    public LoadAbout(AboutListener aboutListener) {
        this.aboutListener = aboutListener;
    }

    @Override
    protected void onPreExecute() {
        aboutListener.onStart();
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject mainJson = new JSONObject(JsonUtils.getJSONString(strings[0]));
            JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);
            JSONObject c;
            for (int i = 0; i < jsonArray.length(); i++) {
                c = jsonArray.getJSONObject(i);

                String appname = c.getString("app_name");
                String applogo = c.getString("app_logo");
                String desc = c.getString("app_description");
                String appversion = c.getString("app_version");
                String appauthor = c.getString("app_author");
                String appcontact = c.getString("app_contact");
                String email = c.getString("app_email");
                String website = c.getString("app_website");
                String privacy = c.getString("app_privacy_policy");
                String developedby = c.getString("app_developed_by");

                //Constant.ad_banner_id = c.getString("banner_ad_id");
                //Constant.ad_inter_id = c.getString("interstital_ad_id");
                Constant.isBannerAd = Boolean.parseBoolean(c.getString("banner_ad"));
                Constant.isInterAd = Boolean.parseBoolean(c.getString("interstital_ad"));
                //Constant.ad_publisher_id = c.getString("publisher_id");
                Constant.adShow = Integer.parseInt(c.getString("interstital_ad_click"));
                Constant.fb_url = c.getString("app_fb_url");
                Constant.twitter_url = c.getString("app_twitter_url");

                Constant.itemAbout = new ItemAbout(appname, applogo, desc, appversion, appauthor, appcontact, email, website, privacy, developedby);
            }
            return "1";
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }
    }

    @Override
    protected void onPostExecute(String s) {
        aboutListener.onEnd(s);
        super.onPostExecute(s);
    }
}
