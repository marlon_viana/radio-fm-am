package com.radiofmam.AsyncTasks;

import android.os.AsyncTask;

import com.radiofmam.interfaces.RadioListListener;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.JsonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoadRadioList extends AsyncTask <String, String, String> {

    private RadioListListener radioListListener;
    private ArrayList<ItemRadio> arrayList;

    public LoadRadioList(RadioListListener radioListListener) {
        this.radioListListener = radioListListener;
        arrayList = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        radioListListener.onStart();
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject mainJson = new JSONObject(JsonUtils.getJSONString(strings[0]));
            JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);
            JSONObject objJson;
            for (int i = 0; i < jsonArray.length(); i++) {
                objJson = jsonArray.getJSONObject(i);

                String id = objJson.getString(Constant.RADIO_ID);
                String name = objJson.getString(Constant.RADIO_NAME);
                String url = objJson.getString(Constant.RADIO_IMAGE);
                String freq = objJson.getString(Constant.RADIO_FREQ);
                String img = objJson.getString(Constant.RADIO_IMAGE_BIG);
                String views = objJson.getString(Constant.RADIO_VIEWS);
                String lang = objJson.getString(Constant.RADIO_LANG);
                String description = objJson.getString(Constant.RADIO_DESC);
                String cityname = "", cid="";
                if(objJson.has(Constant.CITY_CID)) {
                    cid = objJson.getString(Constant.CITY_CID);
                }
                if(objJson.has(Constant.CITY_NAME)) {
                    cityname = objJson.getString(Constant.CITY_NAME);
                }
                ItemRadio objItem = new ItemRadio(id,name,url,freq,img,views,cid,cityname,lang, description,"radio");
                arrayList.add(objItem);
            }
            return "1";
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }

    }

    @Override
    protected void onPostExecute(String s) {
        radioListListener.onEnd(s, arrayList);
        super.onPostExecute(s);
    }
}
