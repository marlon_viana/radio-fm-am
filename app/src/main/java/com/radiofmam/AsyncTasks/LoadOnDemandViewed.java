package com.radiofmam.AsyncTasks;

import android.os.AsyncTask;

import com.radiofmam.interfaces.RadioViewListener;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.JsonUtils;

import org.json.JSONArray;
import org.json.JSONObject;

public class LoadOnDemandViewed extends AsyncTask<String, String, String> {

    private RadioViewListener radioViewListener;

    public LoadOnDemandViewed(RadioViewListener radioViewListener) {
        this.radioViewListener = radioViewListener;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JsonUtils.getJSONString(strings[0]);
            return "1";
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }

    }

    @Override
    protected void onPostExecute(String s) {
        radioViewListener.onEnd(s);
        super.onPostExecute(s);
    }
}
