package com.radiofmam.AsyncTasks;

import android.os.AsyncTask;

import com.radiofmam.interfaces.HomeListener;
import com.radiofmam.item.ItemOnDemandCat;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.utils.Constant;
import com.radiofmam.utils.JsonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoadHome extends AsyncTask<String, String, String> {

    private HomeListener homeListener;
    private ArrayList<ItemRadio> arrayList_featured, arrayList_mostviewed;
    private ArrayList<ItemOnDemandCat> arraylist_ondemand_cat;

    public LoadHome(HomeListener homeListener) {
        this.homeListener = homeListener;
        arrayList_mostviewed = new ArrayList<>();
        arrayList_featured = new ArrayList<>();
        arraylist_ondemand_cat = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        homeListener.onStart();
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject mainJson = new JSONObject(JsonUtils.getJSONString(strings[0]));
            JSONObject jsonObject = mainJson.getJSONObject(Constant.TAG_ROOT);

            JSONArray jsonArray;
            JSONObject objJson;

            jsonArray = jsonObject.getJSONArray(Constant.TAG_FEATURED);
            for (int i = 0; i < jsonArray.length(); i++) {
                objJson = jsonArray.getJSONObject(i);

                String id = objJson.getString(Constant.RADIO_ID);
                String name = objJson.getString(Constant.RADIO_NAME);
                String url = objJson.getString(Constant.RADIO_IMAGE);
                String freq = objJson.getString(Constant.RADIO_FREQ);
                String img = objJson.getString(Constant.RADIO_IMAGE_BIG);
                String views = objJson.getString(Constant.RADIO_VIEWS);
                String cid = objJson.getString(Constant.CITY_CID);
                String cityname = objJson.getString(Constant.CITY_NAME);
                String lang = objJson.getString(Constant.RADIO_LANG);
                String description = objJson.getString(Constant.RADIO_DESC);
                ItemRadio objItem = new ItemRadio(id, name, url, freq, img, views, cid, cityname, lang, description,"radio");
                arrayList_featured.add(objItem);
            }

            jsonArray = jsonObject.getJSONArray(Constant.TAG_MOST_VIEWED);
            for (int i = 0; i < jsonArray.length(); i++) {
                objJson = jsonArray.getJSONObject(i);

                String id = objJson.getString(Constant.RADIO_ID);
                String name = objJson.getString(Constant.RADIO_NAME);
                String url = objJson.getString(Constant.RADIO_IMAGE);
                String freq = objJson.getString(Constant.RADIO_FREQ);
                String img = objJson.getString(Constant.RADIO_IMAGE_BIG);
                String views = objJson.getString(Constant.RADIO_VIEWS);
                String cid = objJson.getString(Constant.CITY_CID);
                String cityname = objJson.getString(Constant.CITY_NAME);
                String lang = objJson.getString(Constant.RADIO_LANG);
                String description = objJson.getString(Constant.RADIO_DESC);
                ItemRadio objItem = new ItemRadio(id, name, url, freq, img, views, cid, cityname, lang, description,"radio");
                arrayList_mostviewed.add(objItem);
            }

            jsonArray = jsonObject.getJSONArray(Constant.TAG_ON_DEMAND_CAT_LIST);
            for (int i = 0; i < jsonArray.length(); i++) {
                objJson = jsonArray.getJSONObject(i);

                String id = objJson.getString(Constant.CAT_ID);
                String name = objJson.getString(Constant.CAT_NAME);
                String image = objJson.getString(Constant.CAT_IMAGE);
                String thumb = objJson.getString(Constant.CAT_THUMB);
                String total_items = objJson.getString(Constant.TOTAL_ITEMS);
                ItemOnDemandCat objItem = new ItemOnDemandCat(id, name,image,thumb,total_items);
                arraylist_ondemand_cat.add(objItem);
            }
            return "1";
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }

    }

    @Override
    protected void onPostExecute(String s) {
        homeListener.onEnd(s, arrayList_featured, arrayList_mostviewed, arraylist_ondemand_cat);
        super.onPostExecute(s);
    }
}
