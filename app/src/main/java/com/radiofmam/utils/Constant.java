package com.radiofmam.utils;

import android.media.MediaPlayer;

import com.google.android.exoplayer2.ExoPlayer;
import com.radiofmam.item.ItemAbout;
import com.radiofmam.item.ItemCity;
import com.radiofmam.item.ItemLanguage;
import com.radiofmam.item.ItemRadio;
import com.radiofmam.freebroadcasters.BuildConfig;

import java.io.Serializable;
import java.util.ArrayList;

import co.mobiwise.library.radio.RadioManager;

public class Constant implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String SERVER_URL = "http://levakromusic.com/radio/";

    public static final String CITY_URL = SERVER_URL + "api.php?city_list";
    public static final String URL_LANGUAGE = SERVER_URL + "api.php?lang_list";
    public static final String RADIOLISTlATEST_URL = SERVER_URL + "api.php?latest";
    public static final String URL_HOME = SERVER_URL + "api.php?home";
    public static final String URL_ON_DEMAND_CAT = SERVER_URL + "api.php?on_demand_cat";
    public static final String URL_ON_DEMAND = SERVER_URL + "api.php?on_demand_cat_id=";
    public static final String URL_MOST_VIEWED = SERVER_URL + "api.php?most_view";
    public static final String URL_SEARCH = SERVER_URL + "api.php?search_text=";
    public static final String URL_THEME = SERVER_URL + "api.php?themes";
    public static final String URL_FEATURED_RADIO = SERVER_URL + "api.php?featured_radio";
    public static final String RADIO_BY_CITY_URL_1 = SERVER_URL + "api.php?city_id=";
    public static final String RADIO_BY_CITY_URL_2 = "&quantity=";
    public static final String RADIO_BY_LANGUAGE_URL_1 = SERVER_URL + "api.php?lang_id=";
    public static final String RADIO_BY_LANGUAGE_URL_2 = "&quantity=";
    public static final String RADIO_URL = SERVER_URL + "api.php?radio_id=";
    public static final String ON_DEMAND_SINGLE_URL = SERVER_URL + "api.php?on_demand_single=";
    public static final String APP_DETAILS_URL = SERVER_URL + "api.php";
    public static final String URL_ABOUT_US_LOGO = SERVER_URL + "images/";

    public static final String TAG_ROOT = "ONLINE_RADIO";
    public static final String TAG_FEATURED = "featured_list";
    public static final String TAG_MOST_VIEWED = "most_view_list";
    public static final String TAG_ON_DEMAND_CAT_LIST = "ondemand_cat_list";
    public static final String RADIO_ID = "id";
    public static final String RADIO_NAME = "radio_name";
    public static final String RADIO_IMAGE_BIG = "video_thumbnail_b";
    public static final String RADIO_IMAGE_SMALL = "video_thumbnail_s";
    public static final String RADIO_IMAGE = "radio_url";
    public static final String RADIO_FREQ = "radio_frequency";
    public static final String RADIO_VIEWS = "views";
    public static final String RADIO_LANG = "language_name";
    public static final String RADIO_DESC = "radio_description";

    public static final String CITY_NAME = "city_name";
    public static final String CITY_CID = "cid";
    public static final String LANG_NAME = "language_name";
    public static final String LANG_ID = "lid";
    public static final String CITY_FLAG = "city_flag";

    public static final String CAT_ID = "cid";
    public static final String CAT_NAME = "category_name";
    public static final String CAT_IMAGE = "category_image";
    public static final String CAT_THUMB = "category_image_thumb";
    public static final String TOTAL_ITEMS = "total_items";

    public static ItemCity itemCity;
    public static ItemLanguage itemLanguage;
    public static String song_name;

    public static int columnWidth = 0;
    // Number of columns of Grid View
    public static final int NUM_OF_COLUMNS = 1;
    // Gridview image padding
    public static final int GRID_PADDING = 7; // in dp
    public static ItemAbout itemAbout;
    public static RadioManager mRadioManager;
    public static MediaPlayer mediaPlayer;
    public static ExoPlayer exoPlayer_ondemand;
    public static String fragment = "Home", pushRID = "0";
    public static Boolean isPlaying = false;
    public static Boolean isBannerAd = true, isInterAd = true, isFromPush = false , isThemeChanged = true, playTypeRadio = true, isPlayed = false, isQuitDialog = false;

    public static ItemRadio itemRadio;
    public static ArrayList<ItemRadio> arrayList_radio = new ArrayList<>();
    public static int pos = 0;
    public static String search_text = "";

    public static String fb_url = "https://www.facebook.com/";
    public static String twitter_url = "https://twitter.com/";
    public static String ad_publisher_id = "pub-4691213171381908";
    public static String ad_banner_id = "ca-app-pub-8059669329564617/5263840719";
    public static String ad_inter_id = "ca-app-pub-8059669329564617/8560103800";

    public static int adShow = 4;
    public static int adCount = 0;
}