package com.radiofmam.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.radiofmam.item.ItemAbout;
import com.radiofmam.item.ItemRadio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "radio.db";
    private SQLiteDatabase db;
    private final Context context;
    private String DB_PATH;
    String outFileName = "";
    SharedPreferences.Editor spEdit;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.context = context;
        DB_PATH = "/data/data/" + context.getPackageName() + "/" + "databases/";
    }


    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();
        //------------------------------------------------------------
        PackageInfo pinfo = null;
        if (!dbExist) {
            getReadableDatabase();
            copyDataBase();
        }

    }

    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    private void copyDataBase() throws IOException {

        InputStream myInput = context.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public Cursor getData(String Query) {
        String myPath = DB_PATH + DB_NAME;
        Cursor c = null;
        try {
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
            c = db.rawQuery(Query, null);
        } catch (Exception e) {
            Log.e("Err", e.toString());
        }
        return c;
    }

    //UPDATE temp_dquot SET age='20',name1='--',rdt='11/08/2014',basic_sa='100000',plno='814',pterm='20',mterm='20',mat_date='11/08/2034',mode='YLY',dab_sa='100000',tr_sa='0',cir_sa='',bonus_rate='42',prem='5276',basic_prem='5118',dab_prem='100.0',step_rate='for Life',loyal_rate='0',bonus_rate='42',act_mat='1,88,000',mly_b_pr='448',qly_b_pr='1345',hly_b_pr='2664',yly_b_pr='5276'  WHERE uniqid=1
    public void dml(String Query) {
        String myPath = DB_PATH + DB_NAME;
        if (db == null)
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        try {
            db.execSQL(Query);
        } catch (Exception e) {
            Log.e("Error", e.toString());
        }
    }

    public Boolean addORremoveFav(ItemRadio itemRadio) {
        Cursor cursor = getData("select * from radio where rid = '"+itemRadio.getRadioId()+"'");
        if(cursor.getCount() == 0) {
            String a = DatabaseUtils.sqlEscapeString(itemRadio.getDescription());
            dml("insert into radio (rid,name,url,freq,image,views,cid,cname, lang, desc) values ('"+itemRadio.getRadioId()+"'," +
                    "'"+itemRadio.getRadioName()+"','"+itemRadio.getRadiourl()+"','"+itemRadio.getRadioFreq()+"'," +
                    "'"+itemRadio.getRadioImageurl()+"','"+itemRadio.getViews()+"','"+itemRadio.getCityId()+"'," +
                    "'"+itemRadio.getCityName()+"', '"+itemRadio.getLanguage()+"', "+a+")");
            return true;
        } else {
            removeFav(itemRadio.getRadioId());
            return false;
        }
    }

    public void removeFav(String rid) {
        dml("delete from radio where rid = '"+rid+"'");
    }

    public ArrayList<ItemRadio> getAllData()
    {
        ArrayList<ItemRadio> arrayList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM radio";

        Cursor cursor = getData(selectQuery);

        // looping through all rows and adding to list
        if (cursor.moveToFirst())
        {
            do {
                String rid = cursor.getString(cursor.getColumnIndex("rid"));
                String rname = cursor.getString(cursor.getColumnIndex("name"));
                String url = cursor.getString(cursor.getColumnIndex("url"));
                String freq = cursor.getString(cursor.getColumnIndex("freq"));
                String image = cursor.getString(cursor.getColumnIndex("image"));
                String views = cursor.getString(cursor.getColumnIndex("views"));
                String cid = cursor.getString(cursor.getColumnIndex("cid"));
                String cname = cursor.getString(cursor.getColumnIndex("cname"));
                String lang = cursor.getString(cursor.getColumnIndex("lang"));
                String desc = cursor.getString(cursor.getColumnIndex("desc"));

                ItemRadio itemRadio = new ItemRadio(rid,rname,url,freq,image,views,cid,cname,lang, desc,"radio");
                arrayList.add(itemRadio);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public Boolean checkFav(String rid) {
        String selectQuery = "SELECT  * FROM radio where rid = '"+rid+"'";
        Cursor cursor = getData(selectQuery);

        if(cursor!=null && cursor.getCount()>0) {
            return true;
        } else {
            return false;
        }
    }

    public void updateViews(String rid, String views)
    {
        String selectQuery = "update radio set views = '"+views+"' where rid = '"+rid+"'";
        dml(selectQuery);
    }

    public void addtoAbout() {
        try {
            dml("delete from about");
            dml("insert into about (name,logo,version,author,contact,email,website,desc,developed,privacy, ad_pub, ad_banner, ad_inter, isbanner, isinter, click, fb_url, twitter_url) values (" +
                    "'" + Constant.itemAbout.getAppName() + "','" + Constant.itemAbout.getAppLogo() + "','" + Constant.itemAbout.getAppVersion() + "'" +
                    ",'" + Constant.itemAbout.getAuthor() + "','" + Constant.itemAbout.getContact() + "','" + Constant.itemAbout.getEmail() + "'" +
                    ",'" + Constant.itemAbout.getWebsite() + "','" + Constant.itemAbout.getAppDesc() + "','" + Constant.itemAbout.getDevelopedby() + "'" +
                    ",'" + Constant.itemAbout.getPrivacy() + "','" + Constant.ad_publisher_id + "','" + Constant.ad_banner_id + "','" + Constant.ad_inter_id + "'" +
                    ",'" + Constant.isBannerAd + "','" + Constant.isInterAd + "','" + Constant.adShow + "','" + Constant.fb_url + "','" + Constant.twitter_url + "')");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Boolean getAbout() {
        String selectQuery = "SELECT * FROM about";

        Cursor c = getData(selectQuery);

        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                String appname = c.getString(c.getColumnIndex("name"));
                String applogo = c.getString(c.getColumnIndex("logo"));
                String desc = c.getString(c.getColumnIndex("desc"));
                String appversion = c.getString(c.getColumnIndex("version"));
                String appauthor = c.getString(c.getColumnIndex("author"));
                String appcontact = c.getString(c.getColumnIndex("contact"));
                String email = c.getString(c.getColumnIndex("email"));
                String website = c.getString(c.getColumnIndex("website"));
                String privacy = c.getString(c.getColumnIndex("privacy"));
                String developedby = c.getString(c.getColumnIndex("developed"));

                //Constant.ad_banner_id = c.getString(c.getColumnIndex("ad_banner"));
                //Constant.ad_inter_id = c.getString(c.getColumnIndex("ad_inter"));
                Constant.isBannerAd = Boolean.parseBoolean(c.getString(c.getColumnIndex("isbanner")));
                Constant.isInterAd = Boolean.parseBoolean(c.getString(c.getColumnIndex("isinter")));
                //Constant.ad_publisher_id = c.getString(c.getColumnIndex("ad_pub"));
                Constant.adShow = Integer.parseInt(c.getString(c.getColumnIndex("click")));
                Constant.fb_url = c.getString(c.getColumnIndex("fb_url"));
                Constant.twitter_url = c.getString(c.getColumnIndex("twitter_url"));

                Constant.itemAbout = new ItemAbout(appname, applogo, desc, appversion, appauthor, appcontact, email, website, privacy, developedby);
            }
            c.close();
            return true;
        } else {
            return false;
        }
    }



    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            case 1:
                String myPath = DB_PATH + DB_NAME;
                if (db == null) {
                    db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
                }
                db.execSQL("ALTER TABLE radio ADD lang TEXT");
            case 2:
                Log.e("aaa - ", "update db");
                db.execSQL("ALTER TABLE about ADD 'ad_pub' TEXT");
                db.execSQL("ALTER TABLE about ADD 'ad_banner' TEXT");
                db.execSQL("ALTER TABLE about ADD 'ad_inter' TEXT");
                db.execSQL("ALTER TABLE about ADD 'isbanner' TEXT");
                db.execSQL("ALTER TABLE about ADD 'isinter' TEXT");
                db.execSQL("ALTER TABLE about ADD 'click' TEXT");
                db.execSQL("ALTER TABLE about ADD 'fb_url' TEXT");
                db.execSQL("ALTER TABLE about ADD 'twitter_url' TEXT");

                db.execSQL("ALTER TABLE radio ADD 'desc' TEXT");
        }
    }
}  